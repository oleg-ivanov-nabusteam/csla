﻿using System;
using Csla;

namespace Ignyte.SeeMyCounselor.BusinessLayer
{
    [Serializable]
    public class StudentAppointmentList : ReadOnlyListBase<StudentAppointmentList, StudentAppointment>
    {
        public static StudentAppointmentList GetStudentAppointmentList(Guid studentKey, DateTime appointmentDate)
        {
            var criteria = new Criteria
            {
                StudentKey = studentKey,
                AppointmentDate = appointmentDate
            };

            return DataPortal.Fetch<StudentAppointmentList>(criteria);
        }

        private void DataPortal_Fetch(Criteria criteria)
        {
            var rlce = RaiseListChangedEvents;
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            DataLayer.Custom.StudentAppointment.CommandTimeOut = 20;
            var studentAppointments = DataLayer.Custom.StudentAppointment.GetStudentAppointments(criteria.StudentKey,
                criteria.AppointmentDate);

            foreach (var studentAppointment in studentAppointments)
            {
                Add(DataPortal.FetchChild<StudentAppointment>(studentAppointment));
            }

            IsReadOnly = true;
            RaiseListChangedEvents = rlce;
        }
    }

    [Serializable]
    public class Criteria : CriteriaBase<Criteria>
    {
        public static readonly PropertyInfo<DateTime> AppointmentDateProperty =
            RegisterProperty<DateTime>(c => c.AppointmentDate);

        public static readonly PropertyInfo<Guid> StudentKeyProperty =
            RegisterProperty<Guid>(c => c.StudentKey);

        public DateTime AppointmentDate
        {
            get { return ReadProperty(AppointmentDateProperty); }
            set { LoadProperty(AppointmentDateProperty, value); }
        }

        public Guid StudentKey
        {
            get { return ReadProperty(StudentKeyProperty); }
            set { LoadProperty(StudentKeyProperty, value); }
        }
    }
}

﻿using System;
using Csla;

namespace Ignyte.SeeMyCounselor.BusinessLayer
{
    [Serializable]
    public class StudentAppointment : ReadOnlyBase<StudentAppointment>
    {
        public static PropertyInfo<Guid> AppointmentKeyProperty = RegisterProperty<Guid>(a => a.AppointmentKey);

        public static PropertyInfo<DateTime> DateTimeScheduledFromProperty =
            RegisterProperty<DateTime>(a => a.DateTimeScheduledFrom);

        public static PropertyInfo<DateTime> DateTimeScheduledToProperty =
            RegisterProperty<DateTime>(a => a.DateTimeScheduledTo);

        public static PropertyInfo<string> CounselorFirstNameProperty =
            RegisterProperty<string>(a => a.CounselorFirstName);

        public static PropertyInfo<string> CounselorLastNameProperty =
            RegisterProperty<string>(a => a.CounselorLastName);

        public static PropertyInfo<string> AppointmentTypeDescriptionProperty =
            RegisterProperty<string>(a => a.AppointmentTypeDescription);

        public static PropertyInfo<string> SupportedColorNameProperty =
            RegisterProperty<string>(a => a.SupportedColorName);

        public Guid AppointmentKey
        {
            get { return GetProperty(AppointmentKeyProperty); }
            private set { LoadProperty(AppointmentKeyProperty, value); }
        }

        public DateTime DateTimeScheduledFrom
        {
            get { return GetProperty(DateTimeScheduledFromProperty); }
            private set { LoadProperty(DateTimeScheduledFromProperty, value); }
        }

        public DateTime DateTimeScheduledTo
        {
            get { return GetProperty(DateTimeScheduledToProperty); }
            private set { LoadProperty(DateTimeScheduledToProperty, value); }
        }

        public string CounselorFirstName
        {
            get { return GetProperty(CounselorFirstNameProperty); }
            private set { LoadProperty(CounselorFirstNameProperty, value); }
        }

        public string CounselorLastName
        {
            get { return GetProperty(CounselorLastNameProperty); }
            private set { LoadProperty(CounselorLastNameProperty, value); }
        }

        public string AppointmentTypeDescription
        {
            get { return GetProperty(AppointmentTypeDescriptionProperty); }
            private set { LoadProperty(AppointmentTypeDescriptionProperty, value); }
        }

        public string SupportedColorName
        {
            get { return GetProperty(SupportedColorNameProperty); }
            private set { LoadProperty(SupportedColorNameProperty, value); }
        }

        private void Child_Fetch(DataLayer.Custom.StudentAppointment studentAppointment)
        {
            AppointmentKey = studentAppointment.AppointmentKey;
            DateTimeScheduledFrom = studentAppointment.DateTimeScheduledFrom;
            DateTimeScheduledTo = studentAppointment.DateTimeScheduledTo;
            CounselorFirstName = studentAppointment.CounselorFirstName;
            CounselorLastName = studentAppointment.CounselorLastName;
            AppointmentTypeDescription = studentAppointment.AppointmentTypeDescription;
            SupportedColorName = studentAppointment.SupportedColorName;
        }
    }
}

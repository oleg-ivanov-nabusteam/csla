//
// Enum		:	EnumTypeOperation.cs
// Author	:  	Ignyte Software © 2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:06 PM
//

using System.Collections;

namespace Ignyte.SeeMyCounselor.DataLayer
{
	/// <summary>
	/// Enumeration of the types of operations
	/// </summary>
	public enum TypeOperation
	{
		Like,
		Less,
		Greater,
		Equal,
		NotEqual
	}
	
  public enum DMLOperation
    {
        Insert,
        Update,
        Delete
    }
	
	/// <summary>
	/// This class returns a string that match the enum TypeOperation
	/// </summary>
	public static class OperationCollection
	{
		public static readonly Hashtable Operation = new Hashtable() { { 0, "Like" }, { 1, "<" }, { 2, ">" }, { 3, "=" }, { 4, "<>" } };
		public const string SEPARATOR = "\n";
	}
}


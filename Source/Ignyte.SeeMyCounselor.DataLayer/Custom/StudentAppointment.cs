﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Ignyte.SeeMyCounselor.DataLayer.Custom
{
    public class StudentAppointmentFields
    {
        public const string AppointmentKey = "AppointmentKey";
        public const string DateTimeScheduledFrom = "DateTimeScheduledFrom";
        public const string DateTimeScheduledTo = "DateTimeScheduledTo";
        public const string CounselorFirstName = "CounselorFirstName";
        public const string CounselorLastName = "CounselorLastName";
        public const string AppointmentTypeDescription = "AppointmentTypeDescription";
        public const string SupportedColorName = "SupportedColorName";
    }

    public class StudentAppointment
    {
        #region DatabaseHelper Properties

        public static int CommandTimeOut { get; set; }

        #endregion

        public Guid AppointmentKey { get; set; }
        public DateTime DateTimeScheduledFrom { get; set; }
        public DateTime DateTimeScheduledTo { get; set; }
        public string CounselorFirstName { get; set; }
        public string CounselorLastName { get; set; }
        public string AppointmentTypeDescription { get; set; }
        public string SupportedColorName { get; set; }

        public static IList<StudentAppointment> GetStudentAppointments(Guid studentKey, DateTime appointmentDate)
        {
            var oDatabaseHelper = new DatabaseHelper {CommandTimeOut = CommandTimeOut};
            var executionState = false;

            oDatabaseHelper.AddParameter("@StudentKey", studentKey);
            oDatabaseHelper.AddParameter("@AppointmentDate", appointmentDate);
            oDatabaseHelper.AddParameter("@dlgErrorCode", -1, ParameterDirection.Output);

            var dr = oDatabaseHelper.ExecuteReader("csp_StudentAppointment_GetStudentAppointments", ref executionState);

            var studentAppointments = GetStudentAppointmentsFromReader(dr, oDatabaseHelper);

            dr.Close();
            oDatabaseHelper.Dispose();

            return studentAppointments;
        }

        internal static IList<StudentAppointment> GetStudentAppointmentsFromReader(IDataReader rdr,
            DatabaseHelper oDatabaseHelper)
        {
            var studentAppointments = new List<StudentAppointment>();

            while (rdr.Read())
            {
                var studentAppointment = new StudentAppointment
                {
                    AppointmentKey = rdr.GetGuid(rdr.GetOrdinal(StudentAppointmentFields.AppointmentKey)),
                    DateTimeScheduledFrom =
                        rdr.GetDateTime(rdr.GetOrdinal(StudentAppointmentFields.DateTimeScheduledFrom)),
                    DateTimeScheduledTo = rdr.GetDateTime(rdr.GetOrdinal(StudentAppointmentFields.DateTimeScheduledTo)),
                    CounselorFirstName = rdr.GetString(rdr.GetOrdinal(StudentAppointmentFields.CounselorFirstName)),
                    CounselorLastName = rdr.GetString(rdr.GetOrdinal(StudentAppointmentFields.CounselorLastName)),
                    AppointmentTypeDescription =
                        rdr.GetString(rdr.GetOrdinal(StudentAppointmentFields.AppointmentTypeDescription)),
                    SupportedColorName = rdr.GetString(rdr.GetOrdinal(StudentAppointmentFields.SupportedColorName))
                };

                studentAppointments.Add(studentAppointment);
            }

            return studentAppointments;
        }
    }
}

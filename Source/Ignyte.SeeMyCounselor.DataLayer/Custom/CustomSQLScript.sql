if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[csp_StudentAppointment_GetStudentAppointments]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[csp_StudentAppointment_GetStudentAppointments]
GO

CREATE PROCEDURE [dbo].[csp_StudentAppointment_GetStudentAppointments]
	@AppointmentDate						DATETIME,
	@StudentKey								UNIQUEIDENTIFIER,
	@dlgErrorCode							INT OUTPUT
AS
SET NOCOUNT ON

SELECT 
	TAppointment.[Key]						AppointmentKey,
	TAppointment.DateTimeScheduledFrom		DateTimeScheduledFrom,
	TAppointment.DateTimeScheduledTo		DateTimeScheduledTo,
	TUser.FirstName							CounselorFirstName,
	TUser.LastName							CounselorLastName,
	TAppointmentType.Description			AppointmentTypeDescription,
	TSupportedColor.Name					SupportedColorName
FROM
	TAppointment
	JOIN TUser
	ON TAppointment.CounselorTUserKey = TUser.[Key]
	JOIN TAppointmentType
	ON TAppointment.TAppointmentTypeKey = TAppointmentType.[Key]
	JOIN TSupportedColor
	ON TAppointmentType.TSupportedColorKey = TSupportedColor.[Key]
WHERE
	TStudentKey = @StudentKey AND
	DATEADD(DD, 0, DATEDIFF(DD, 0, TAppointment.DateTimeScheduledFrom)) = DATEADD(DD, 0, DATEDIFF(DD, 0, @AppointmentDate)) AND
	TAppointment.CanceledByTUserKey IS NULL
ORDER BY
	TAppointment.DateTimeScheduledFrom ASC

SET @dlgErrorCode = @@ERROR
GO
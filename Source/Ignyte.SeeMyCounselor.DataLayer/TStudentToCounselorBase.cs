//
// Class	:	TStudentToCounselorBase.cs
// Author	:  	Ignyte Software © 2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:08 PM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Xml;

namespace Ignyte.SeeMyCounselor.DataLayer
{

	/// <summary>
	/// Class for the properties of the object
	/// </summary>
	public class TStudentToCounselorFields
	{
		public const string Key                       = "Key";
		public const string TStudentKey               = "TStudentKey";
		public const string CounselorTUserKey         = "CounselorTUserKey";
		public const string TStamp                    = "TStamp";
		public const string DateCreated               = "DateCreated";
		public const string CreatedBy                 = "CreatedBy";
		public const string LastUpdatedBy             = "LastUpdatedBy";
		public const string Source                    = "Source";
	}
	
	/// <summary>
	/// Data access class for the "TStudentToCounselor" table.
	/// </summary>
	[Serializable]
	public class TStudentToCounselorBase
	{
		
		#region Class Level Variables
		
		private DatabaseHelper oDatabaseHelper = new DatabaseHelper();
    
		private Guid?          	_keyNonDefault           	= null;
		private Guid?          	_tStudentKeyNonDefault   	= null;
		private Guid?          	_counselorTUserKeyNonDefault	= null;
		private DateTime?      	_tStampNonDefault        	= DateTime.Now;
		private DateTime?      	_dateCreatedNonDefault   	= DateTime.Now;
		private string         	_createdByNonDefault     	= null;
		private string         	_lastUpdatedByNonDefault 	= null;
		private string         	_sourceNonDefault        	= null;
		
		#endregion
		
        #region DatabaseHelper Properties
    
		public static int CommandTimeOut
		{
			get; set;
		}

        #endregion
    
        #region Constants
	  	
		#endregion
		
		#region Constructors / Destructors

		/// <summary>
		/// Class Constructor
		///</summary>
		public TStudentToCounselorBase() { }
					
		#endregion
		
		#region Properties

		/// <summary>
		/// Returns the identifier of the persistent object. Mandatory.
		/// </summary>
		public Guid? Key
		{
			get 
			{ 
				return _keyNonDefault;
			}
			set 
			{
			
				_keyNonDefault = value; 
			}
		}

		/// <summary>
		/// The foreign key connected with another persistent object.
		/// </summary>
		public Guid? TStudentKey
		{
			get 
			{ 
				return _tStudentKeyNonDefault;
			}
			set 
			{
			
				_tStudentKeyNonDefault = value; 
			}
		}

		/// <summary>
		/// The foreign key connected with another persistent object.
		/// </summary>
		public Guid? CounselorTUserKey
		{
			get 
			{ 
				return _counselorTUserKeyNonDefault;
			}
			set 
			{
			
				_counselorTUserKeyNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "TStamp" field.  Mandatory.
		/// </summary>
		public DateTime? TStamp
		{
			get 
			{ 
				return _tStampNonDefault;
			}
			set 
			{
			
				_tStampNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "DateCreated" field.  Mandatory.
		/// </summary>
		public DateTime? DateCreated
		{
			get 
			{ 
				return _dateCreatedNonDefault;
			}
			set 
			{
			
				_dateCreatedNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "CreatedBy" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string CreatedBy
		{
			get 
			{ 
				if(_createdByNonDefault==null)return _createdByNonDefault;
				else return _createdByNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("CreatedBy length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_createdByNonDefault =null;//null value 
				}
				else
				{		           
					_createdByNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "LastUpdatedBy" field. Length must be between 0 and 50 characters. 
		/// </summary>
		public string LastUpdatedBy
		{
			get 
			{ 
				if(_lastUpdatedByNonDefault==null)return _lastUpdatedByNonDefault;
				else return _lastUpdatedByNonDefault.Trim(); 
			}
			set 
			{
			    if (value != null && value.Length > 50)
					throw new ArgumentException("LastUpdatedBy length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_lastUpdatedByNonDefault =null;//null value 
				}
				else
				{		           
					_lastUpdatedByNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "Source" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string Source
		{
			get 
			{ 
				if(_sourceNonDefault==null)return _sourceNonDefault;
				else return _sourceNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("Source length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_sourceNonDefault =null;//null value 
				}
				else
				{		           
					_sourceNonDefault = value.Trim(); 
				}
			}
		}		//This property is related to the table name that exist in database
		public static string tableName
		{
			get 
			{ 
				  return "TStudentToCounselor";
			}
		}

		#endregion
		
		#region Methods (Public)

		/// <summary>
		/// This method will insert one new row into the database using the property Information
		/// </summary>
		/// <param name="getBackValues" type="bool">Whether to get the default values inserted, from the database</param>
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool InsertWithDefaultValues(bool getBackValues) 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Key", _keyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			  
			// Pass the value of '_tStudentKey' as parameter 'TStudentKey' of the stored procedure.
			if(_tStudentKeyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStudentKey", _tStudentKeyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStudentKey", DBNull.Value );
			  
			// Pass the value of '_counselorTUserKey' as parameter 'CounselorTUserKey' of the stored procedure.
			if(_counselorTUserKeyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CounselorTUserKey", _counselorTUserKeyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CounselorTUserKey", DBNull.Value );
			  
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			if(_tStampNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStamp", DBNull.Value );
			  
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			if(_dateCreatedNonDefault!=null)
			  oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault);
			else
			  oDatabaseHelper.AddParameter("@DateCreated", DBNull.Value );
			  
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			if(_createdByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CreatedBy", DBNull.Value );
			  
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			if(_lastUpdatedByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", DBNull.Value );
			  
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			if(_sourceNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Source", _sourceNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Source", DBNull.Value );
			  
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			if(!getBackValues )
			{
				oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_Insert_WithDefaultValues", ref ExecutionState);
			}
			else
			{
				DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_Insert_WithDefaultValues_AndReturn", ref ExecutionState);
                try
                {
                    if (dr.Read())
                    {
                        PopulateObjectFromReader(this, dr);
                    }
                    dr.Close();
                }
                catch(Exception ex)
                {
                    dr.Close();
                    throw ex;
                }
			}
			oDatabaseHelper.Dispose();	
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will insert one new row into the database using the property Information
		/// </summary>
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Insert() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Key", _keyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			// Pass the value of '_tStudentKey' as parameter 'TStudentKey' of the stored procedure.
			if(_tStudentKeyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStudentKey", _tStudentKeyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStudentKey", DBNull.Value );
			// Pass the value of '_counselorTUserKey' as parameter 'CounselorTUserKey' of the stored procedure.
			if(_counselorTUserKeyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CounselorTUserKey", _counselorTUserKeyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CounselorTUserKey", DBNull.Value );
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			if(_tStampNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStamp", DBNull.Value );
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			if(_dateCreatedNonDefault!=null)
			  oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault);
			else
			  oDatabaseHelper.AddParameter("@DateCreated", DBNull.Value );
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			if(_createdByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CreatedBy", DBNull.Value );
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			if(_lastUpdatedByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", DBNull.Value );
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			if(_sourceNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Source", _sourceNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Source", DBNull.Value );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_Insert", ref ExecutionState);
			oDatabaseHelper.Dispose();	
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Update one new row into the database using the property Information
		/// </summary>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Update() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			oDatabaseHelper.AddParameter("@Key", _keyNonDefault );
			
			// Pass the value of '_tStudentKey' as parameter 'TStudentKey' of the stored procedure.
			oDatabaseHelper.AddParameter("@TStudentKey", _tStudentKeyNonDefault );
			
			// Pass the value of '_counselorTUserKey' as parameter 'CounselorTUserKey' of the stored procedure.
			oDatabaseHelper.AddParameter("@CounselorTUserKey", _counselorTUserKeyNonDefault );
			
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault );
			
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault );
			
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault );
			
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault );
			
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			oDatabaseHelper.AddParameter("@Source", _sourceNonDefault );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_Update", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete one row from the database using the property Information
		/// </summary>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Delete() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
				oDatabaseHelper.AddParameter("@Key", _keyNonDefault );
			else
				oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_Delete", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete one row from the database using the primary key information
		/// </summary>
		///
		/// <param name="pk" type="TStudentToCounselorPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool Delete(TStudentToCounselorPrimaryKey pk) 
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
   oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
   
			oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_Delete", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="TStudentToCounselorFields">Field of the class TStudentToCounselor</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool DeleteByField(string field, object fieldValue)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TStudentToCounselor_DeleteByField", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will return an object representing the record matching the primary key information specified.
		/// </summary>
		///
		/// <param name="pk" type="TStudentToCounselorPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TStudentToCounselor</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselor SelectOne(TStudentToCounselorPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectbyPrimaryKey", ref ExecutionState);
			if (dr.Read())
			{
				TStudentToCounselor obj=new TStudentToCounselor();	
				PopulateObjectFromReader(obj,dr);
				dr.Close();              
				oDatabaseHelper.Dispose();
				return obj;
			}
			else
			{
				dr.Close();
				oDatabaseHelper.Dispose();
				return null;
			}
			
		}

		/// <summary>
		/// This method will return a list of objects representing all records in the table.
		/// </summary>
		///
		/// <returns>list of objects of class TStudentToCounselor in the form of object of TStudentToCounselors </returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAll()
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAll", ref ExecutionState);
			TStudentToCounselors TStudentToCounselors = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TStudentToCounselors;
			
		}

		/// <summary>
		/// Deprecated. Use SelectByField(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation) instead. This method will get row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TStudentToCounselor</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		///
		/// <returns>List of object of class TStudentToCounselor in the form of an object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectByField(string field, object fieldValue)
		{

			

			
			
			
			

			return SelectByField(field, fieldValue, null, TypeOperation.Equal);
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TStudentToCounselor</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		/// <param name="fieldValue2" type="object">Value for the field specified.</param>
		/// <param name="typeOperation" type="TypeOperation">Operator that is used if fieldValue2=null or fieldValue2="".</param>
		///
		/// <returns>List of object of class TStudentToCounselor in the form of an object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectByField(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			oDatabaseHelper.AddParameter("@Value2", fieldValue2 );
			oDatabaseHelper.AddParameter("@Operation", OperationCollection.Operation[(int)typeOperation] );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectByField", ref ExecutionState);
			TStudentToCounselors TStudentToCounselors = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TStudentToCounselors;
			
		}

		/// <summary>
		/// This method will return a list of objects representing the specified number of entries from the specified record number in the table.
		/// </summary>
		///
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		/// <param name="orderByStatement" type="string">The field value to number</param>
		///
		/// <returns>list of objects of class TStudentToCounselor in the form of object of TStudentToCounselors </returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAllPaged(int? pageSize, int? skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages);
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllPaged", ref ExecutionState);
			TStudentToCounselors TStudentToCounselors = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TStudentToCounselors;
			
		}

		/// <summary>
		/// This method will return a list of objects representing the specified number of entries from the specified record number in the table 
		/// using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TStudentToCounselor</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		/// <param name="fieldValue2" type="object">Value for the field specified.</param>
		/// <param name="typeOperation" type="TypeOperation">Operator that is used if fieldValue2=null or fieldValue2="".</param>
		/// <param name="orderByStatement" type="string">The field value to number.</param>
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		///
		/// <returns>List of object of class TStudentToCounselor in the form of an object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectByFieldPaged(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation, int pageSize, int skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			oDatabaseHelper.AddParameter("@Value2", fieldValue2 );
			oDatabaseHelper.AddParameter("@Operation", OperationCollection.Operation[(int)typeOperation] );
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages );
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectByFieldPaged", ref ExecutionState);
			TStudentToCounselors TStudentToCounselors = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TStudentToCounselors;
			
		}

		/// <summary>
		/// This method will return a count all records in the table.
		/// </summary>
		///
		/// <returns>count records</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static int SelectAllCount()
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();			
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr=oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllCount", ref ExecutionState);
			int count = 0;
            using (DataTable dt = new DataTable())
            {
                dt.Load(dr);
                count = Convert.ToInt32(dt.Rows[0][0]);
            }
			oDatabaseHelper.Dispose();
			return count;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TStudentPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAllByForeignKeyTStudentKey(TStudentPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TStudentToCounselors obj = null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey", ref ExecutionState);
			obj = new TStudentToCounselors();
			obj = TStudentToCounselor.PopulateObjectsFromReaderWithCheckingReader(dr, oDatabaseHelper);
			
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TStudentPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		/// <param name="orderByStatement" type="string">The field value to number</param>
		///
		/// <returns>object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAllByForeignKeyTStudentKeyPaged(TStudentPrimaryKey pk, int pageSize, int skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TStudentToCounselors obj = null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages);
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKeyPaged", ref ExecutionState);
			obj = new TStudentToCounselors();
			obj = TStudentToCounselor.PopulateObjectsFromReaderWithCheckingReader(dr, oDatabaseHelper);
			
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will delete row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TStudentPrimaryKey">Primary Key information based on which data is to be deleted.</param>
		///
		/// <returns>object of boolean type as an indicator for operation success .</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool DeleteAllByForeignKeyTStudentKey(TStudentPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteNonQuery("gsp_TStudentToCounselor_DeleteAllByForeignKeyTStudentKey", ref ExecutionState);
			
      oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}



		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAllByForeignKeyCounselorTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TStudentToCounselors obj = null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey", ref ExecutionState);
			obj = new TStudentToCounselors();
			obj = TStudentToCounselor.PopulateObjectsFromReaderWithCheckingReader(dr, oDatabaseHelper);
			
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		/// <param name="orderByStatement" type="string">The field value to number</param>
		///
		/// <returns>object of class TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TStudentToCounselors SelectAllByForeignKeyCounselorTUserKeyPaged(TUserPrimaryKey pk, int pageSize, int skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TStudentToCounselors obj = null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages);
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKeyPaged", ref ExecutionState);
			obj = new TStudentToCounselors();
			obj = TStudentToCounselor.PopulateObjectsFromReaderWithCheckingReader(dr, oDatabaseHelper);
			
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will delete row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be deleted.</param>
		///
		/// <returns>object of boolean type as an indicator for operation success .</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool DeleteAllByForeignKeyCounselorTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteNonQuery("gsp_TStudentToCounselor_DeleteAllByForeignKeyCounselorTUserKey", ref ExecutionState);
			
      oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		#endregion	
		
		#region Methods (Private)

		/// <summary>
		/// tests a string to be a well formed xml or not,
		/// it throws ArgumentException when string text is not well formed.otherwise this 
		/// method is executed silently .
		/// </summary>
		/// <param name="text" type="string">xml string to validate.</param>
		/// <param name="fieldName" type="string">field Name to validate.</param>
		/// <exception cref="System.ArgumentException"> throws ArgumentException when text is not well formed parameter.otherwise this 
		/// method is executed silently .</exception>
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static void IsValidXmlString(string text,string fieldName)
		{
			XmlTextReader r = new XmlTextReader(new StringReader(text));
			try
			{
				while (r.Read())
				{
				  /*do nothing ,just continue as long as xml is valid*/ 
				}
			}
			catch(Exception)
			{
				throw new ArgumentException ("Field ("+fieldName+") xml text argument isn't well formed");				
			}
			finally
			{
				r.Close();
			
			}
		  //end silently(well formed xml)
		}    
		/// <summary>
		/// Populates the fields of a single objects from the columns found in an open reader.
		/// </summary>
		/// <param name="obj" type="TStudentToCounselor">Object of TStudentToCounselor to populate</param>
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static void PopulateObjectFromReader(TStudentToCounselorBase obj,IDataReader rdr) 
		{

			obj.Key = rdr.GetGuid(rdr.GetOrdinal(TStudentToCounselorFields.Key));
			obj.TStudentKey = rdr.GetGuid(rdr.GetOrdinal(TStudentToCounselorFields.TStudentKey));
			obj.CounselorTUserKey = rdr.GetGuid(rdr.GetOrdinal(TStudentToCounselorFields.CounselorTUserKey));
			obj.TStamp = rdr.GetDateTime(rdr.GetOrdinal(TStudentToCounselorFields.TStamp));
			obj.DateCreated = rdr.GetDateTime(rdr.GetOrdinal(TStudentToCounselorFields.DateCreated));
			obj.CreatedBy = rdr.GetString(rdr.GetOrdinal(TStudentToCounselorFields.CreatedBy));
			if (!rdr.IsDBNull(rdr.GetOrdinal(TStudentToCounselorFields.LastUpdatedBy)))
			{
				obj.LastUpdatedBy = rdr.GetString(rdr.GetOrdinal(TStudentToCounselorFields.LastUpdatedBy));
			}
			
			obj.Source = rdr.GetString(rdr.GetOrdinal(TStudentToCounselorFields.Source));

		}

		/// <summary>
		/// Populates the fields for multiple objects from the columns found in an open reader.
		/// </summary>
		///
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <returns>Object of TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static TStudentToCounselors PopulateObjectsFromReader(IDataReader rdr) 
		{
			TStudentToCounselors list = new TStudentToCounselors();
			
			while (rdr.Read())
			{
				TStudentToCounselor obj = new TStudentToCounselor();
				PopulateObjectFromReader(obj,rdr);
				list.Add(obj);
			}
			return list;
			
		}

		/// <summary>
		/// Populates the fields for multiple objects from the columns found in an open reader.
		/// </summary>
		///
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <returns>Object of TStudentToCounselors</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static TStudentToCounselors PopulateObjectsFromReaderWithCheckingReader(IDataReader rdr, DatabaseHelper oDatabaseHelper) 
		{

			TStudentToCounselors list = new TStudentToCounselors();
			
            if (rdr.Read())
			{
				TStudentToCounselor obj = new TStudentToCounselor();
				PopulateObjectFromReader(obj, rdr);
				list.Add(obj);
				while (rdr.Read())
				{
					obj = new TStudentToCounselor();
					PopulateObjectFromReader(obj, rdr);
					list.Add(obj);
				}
				oDatabaseHelper.Dispose();
				return list;
			}
			else
			{
				oDatabaseHelper.Dispose();
				return list;
			}
			
		}

	
	#endregion

	}
}

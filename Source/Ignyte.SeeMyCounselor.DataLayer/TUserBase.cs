//
// Class	:	TUserBase.cs
// Author	:  	Ignyte Software © 2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:08 PM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;
using System.IO;
using System.Xml;

namespace Ignyte.SeeMyCounselor.DataLayer
{

	/// <summary>
	/// Class for the properties of the object
	/// </summary>
	public class TUserFields
	{
		public const string Key                       = "Key";
		public const string FirstName                 = "FirstName";
		public const string LastName                  = "LastName";
		public const string Username                  = "Username";
		public const string Password                  = "Password";
		public const string Email                     = "Email";
		public const string TStamp                    = "TStamp";
		public const string DateCreated               = "DateCreated";
		public const string CreatedBy                 = "CreatedBy";
		public const string LastUpdatedBy             = "LastUpdatedBy";
		public const string Source                    = "Source";
	}
	
	/// <summary>
	/// Data access class for the "TUser" table.
	/// </summary>
	[Serializable]
	public class TUserBase
	{
		
		#region Class Level Variables
		
		private DatabaseHelper oDatabaseHelper = new DatabaseHelper();
    
		private Guid?          	_keyNonDefault           	= null;
		private string         	_firstNameNonDefault     	= null;
		private string         	_lastNameNonDefault      	= null;
		private string         	_usernameNonDefault      	= null;
		private string         	_passwordNonDefault      	= null;
		private string         	_emailNonDefault         	= null;
		private DateTime?      	_tStampNonDefault        	= DateTime.Now;
		private DateTime?      	_dateCreatedNonDefault   	= DateTime.Now;
		private string         	_createdByNonDefault     	= null;
		private string         	_lastUpdatedByNonDefault 	= null;
		private string         	_sourceNonDefault        	= null;

		private TAppointments _tAppointmentsCounselorTUserKey = null;
		private TAppointments _tAppointmentsCanceledByTUserKey = null;
		private TAppointments _tAppointmentsScheduledByTUserKey = null;
		private TSchoolToTUsers _tSchoolToTUsersTUserKey = null;
		private TStudentToCounselors _tStudentToCounselorsCounselorTUserKey = null;
		
		#endregion
		
        #region DatabaseHelper Properties
    
		public static int CommandTimeOut
		{
			get; set;
		}

        #endregion
    
        #region Constants
	  	
		#endregion
		
		#region Constructors / Destructors

		/// <summary>
		/// Class Constructor
		///</summary>
		public TUserBase() { }
					
		#endregion
		
		#region Properties

		/// <summary>
		/// Returns the identifier of the persistent object. Mandatory.
		/// </summary>
		public Guid? Key
		{
			get 
			{ 
				return _keyNonDefault;
			}
			set 
			{
			
				_keyNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "FirstName" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string FirstName
		{
			get 
			{ 
				if(_firstNameNonDefault==null)return _firstNameNonDefault;
				else return _firstNameNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("FirstName length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_firstNameNonDefault =null;//null value 
				}
				else
				{		           
					_firstNameNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "LastName" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string LastName
		{
			get 
			{ 
				if(_lastNameNonDefault==null)return _lastNameNonDefault;
				else return _lastNameNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("LastName length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_lastNameNonDefault =null;//null value 
				}
				else
				{		           
					_lastNameNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "Username" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string Username
		{
			get 
			{ 
				if(_usernameNonDefault==null)return _usernameNonDefault;
				else return _usernameNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("Username length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_usernameNonDefault =null;//null value 
				}
				else
				{		           
					_usernameNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "Password" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string Password
		{
			get 
			{ 
				if(_passwordNonDefault==null)return _passwordNonDefault;
				else return _passwordNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("Password length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_passwordNonDefault =null;//null value 
				}
				else
				{		           
					_passwordNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "Email" field. Length must be between 0 and 100 characters. Mandatory.
		/// </summary>
		public string Email
		{
			get 
			{ 
				if(_emailNonDefault==null)return _emailNonDefault;
				else return _emailNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 100)
					throw new ArgumentException("Email length must be between 0 and 100 characters.");

				
				if(value ==null)
				{
					_emailNonDefault =null;//null value 
				}
				else
				{		           
					_emailNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "TStamp" field.  Mandatory.
		/// </summary>
		public DateTime? TStamp
		{
			get 
			{ 
				return _tStampNonDefault;
			}
			set 
			{
			
				_tStampNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "DateCreated" field.  Mandatory.
		/// </summary>
		public DateTime? DateCreated
		{
			get 
			{ 
				return _dateCreatedNonDefault;
			}
			set 
			{
			
				_dateCreatedNonDefault = value; 
			}
		}

		/// <summary>
		/// This property is mapped to the "CreatedBy" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string CreatedBy
		{
			get 
			{ 
				if(_createdByNonDefault==null)return _createdByNonDefault;
				else return _createdByNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("CreatedBy length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_createdByNonDefault =null;//null value 
				}
				else
				{		           
					_createdByNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "LastUpdatedBy" field. Length must be between 0 and 50 characters. 
		/// </summary>
		public string LastUpdatedBy
		{
			get 
			{ 
				if(_lastUpdatedByNonDefault==null)return _lastUpdatedByNonDefault;
				else return _lastUpdatedByNonDefault.Trim(); 
			}
			set 
			{
			    if (value != null && value.Length > 50)
					throw new ArgumentException("LastUpdatedBy length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_lastUpdatedByNonDefault =null;//null value 
				}
				else
				{		           
					_lastUpdatedByNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// This property is mapped to the "Source" field. Length must be between 0 and 50 characters. Mandatory.
		/// </summary>
		public string Source
		{
			get 
			{ 
				if(_sourceNonDefault==null)return _sourceNonDefault;
				else return _sourceNonDefault.Trim(); 
			}
			set 
			{
			    if (value == null)
					throw new ArgumentNullException("value", "Value is null.");
				   if (value != null && value.Length > 50)
					throw new ArgumentException("Source length must be between 0 and 50 characters.");

				
				if(value ==null)
				{
					_sourceNonDefault =null;//null value 
				}
				else
				{		           
					_sourceNonDefault = value.Trim(); 
				}
			}
		}

		/// <summary>
		/// Provides access to the related table 'TAppointment'
		/// </summary>
		public TAppointments TAppointmentsUsingCounselorTUserKey
		{
			get 
			{
				if (_tAppointmentsCounselorTUserKey == null && Key != null)
				{
					_tAppointmentsCounselorTUserKey = new TAppointments();
					_tAppointmentsCounselorTUserKey = TAppointment.SelectByField("CounselorTUserKey",Key, null, TypeOperation.Equal);
				}                
				return _tAppointmentsCounselorTUserKey; 
			}
			set 
			{
				  _tAppointmentsCounselorTUserKey = value;
			}
		}

		/// <summary>
		/// Provides access to the related table 'TAppointment'
		/// </summary>
		public TAppointments TAppointmentsUsingCanceledByTUserKey
		{
			get 
			{
				if (_tAppointmentsCanceledByTUserKey == null && Key != null)
				{
					_tAppointmentsCanceledByTUserKey = new TAppointments();
					_tAppointmentsCanceledByTUserKey = TAppointment.SelectByField("CanceledByTUserKey",Key, null, TypeOperation.Equal);
				}                
				return _tAppointmentsCanceledByTUserKey; 
			}
			set 
			{
				  _tAppointmentsCanceledByTUserKey = value;
			}
		}

		/// <summary>
		/// Provides access to the related table 'TAppointment'
		/// </summary>
		public TAppointments TAppointmentsUsingScheduledByTUserKey
		{
			get 
			{
				if (_tAppointmentsScheduledByTUserKey == null && Key != null)
				{
					_tAppointmentsScheduledByTUserKey = new TAppointments();
					_tAppointmentsScheduledByTUserKey = TAppointment.SelectByField("ScheduledByTUserKey",Key, null, TypeOperation.Equal);
				}                
				return _tAppointmentsScheduledByTUserKey; 
			}
			set 
			{
				  _tAppointmentsScheduledByTUserKey = value;
			}
		}

		/// <summary>
		/// Provides access to the related table 'TSchoolToTUser'
		/// </summary>
		public TSchoolToTUsers TSchoolToTUsersUsingTUserKey
		{
			get 
			{
				if (_tSchoolToTUsersTUserKey == null && Key != null)
				{
					_tSchoolToTUsersTUserKey = new TSchoolToTUsers();
					_tSchoolToTUsersTUserKey = TSchoolToTUser.SelectByField("TUserKey",Key, null, TypeOperation.Equal);
				}                
				return _tSchoolToTUsersTUserKey; 
			}
			set 
			{
				  _tSchoolToTUsersTUserKey = value;
			}
		}

		/// <summary>
		/// Provides access to the related table 'TStudentToCounselor'
		/// </summary>
		public TStudentToCounselors TStudentToCounselorsUsingCounselorTUserKey
		{
			get 
			{
				if (_tStudentToCounselorsCounselorTUserKey == null && Key != null)
				{
					_tStudentToCounselorsCounselorTUserKey = new TStudentToCounselors();
					_tStudentToCounselorsCounselorTUserKey = TStudentToCounselor.SelectByField("CounselorTUserKey",Key, null, TypeOperation.Equal);
				}                
				return _tStudentToCounselorsCounselorTUserKey; 
			}
			set 
			{
				  _tStudentToCounselorsCounselorTUserKey = value;
			}
		}		//This property is related to the table name that exist in database
		public static string tableName
		{
			get 
			{ 
				  return "TUser";
			}
		}

		#endregion
		
		#region Methods (Public)

		/// <summary>
		/// This method will insert one new row into the database using the property Information
		/// </summary>
		/// <param name="getBackValues" type="bool">Whether to get the default values inserted, from the database</param>
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool InsertWithDefaultValues(bool getBackValues) 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Key", _keyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			  
			// Pass the value of '_firstName' as parameter 'FirstName' of the stored procedure.
			if(_firstNameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@FirstName", _firstNameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@FirstName", DBNull.Value );
			  
			// Pass the value of '_lastName' as parameter 'LastName' of the stored procedure.
			if(_lastNameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastName", _lastNameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastName", DBNull.Value );
			  
			// Pass the value of '_username' as parameter 'Username' of the stored procedure.
			if(_usernameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Username", _usernameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Username", DBNull.Value );
			  
			// Pass the value of '_password' as parameter 'Password' of the stored procedure.
			if(_passwordNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Password", _passwordNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Password", DBNull.Value );
			  
			// Pass the value of '_email' as parameter 'Email' of the stored procedure.
			if(_emailNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Email", _emailNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Email", DBNull.Value );
			  
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			if(_tStampNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStamp", DBNull.Value );
			  
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			if(_dateCreatedNonDefault!=null)
			  oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault);
			else
			  oDatabaseHelper.AddParameter("@DateCreated", DBNull.Value );
			  
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			if(_createdByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CreatedBy", DBNull.Value );
			  
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			if(_lastUpdatedByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", DBNull.Value );
			  
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			if(_sourceNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Source", _sourceNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Source", DBNull.Value );
			  
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			if(!getBackValues )
			{
				oDatabaseHelper.ExecuteScalar("gsp_TUser_Insert_WithDefaultValues", ref ExecutionState);
			}
			else
			{
				DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_Insert_WithDefaultValues_AndReturn", ref ExecutionState);
                try
                {
                    if (dr.Read())
                    {
                        PopulateObjectFromReader(this, dr);
                    }
                    dr.Close();
                }
                catch(Exception ex)
                {
                    dr.Close();
                    throw ex;
                }
			}
			oDatabaseHelper.Dispose();	
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will insert one new row into the database using the property Information
		/// </summary>
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Insert() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Key", _keyNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			// Pass the value of '_firstName' as parameter 'FirstName' of the stored procedure.
			if(_firstNameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@FirstName", _firstNameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@FirstName", DBNull.Value );
			// Pass the value of '_lastName' as parameter 'LastName' of the stored procedure.
			if(_lastNameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastName", _lastNameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastName", DBNull.Value );
			// Pass the value of '_username' as parameter 'Username' of the stored procedure.
			if(_usernameNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Username", _usernameNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Username", DBNull.Value );
			// Pass the value of '_password' as parameter 'Password' of the stored procedure.
			if(_passwordNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Password", _passwordNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Password", DBNull.Value );
			// Pass the value of '_email' as parameter 'Email' of the stored procedure.
			if(_emailNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Email", _emailNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Email", DBNull.Value );
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			if(_tStampNonDefault!=null)
			  oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault);
			else
			  oDatabaseHelper.AddParameter("@TStamp", DBNull.Value );
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			if(_dateCreatedNonDefault!=null)
			  oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault);
			else
			  oDatabaseHelper.AddParameter("@DateCreated", DBNull.Value );
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			if(_createdByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@CreatedBy", DBNull.Value );
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			if(_lastUpdatedByNonDefault!=null)
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault);
			else
			  oDatabaseHelper.AddParameter("@LastUpdatedBy", DBNull.Value );
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			if(_sourceNonDefault!=null)
			  oDatabaseHelper.AddParameter("@Source", _sourceNonDefault);
			else
			  oDatabaseHelper.AddParameter("@Source", DBNull.Value );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TUser_Insert", ref ExecutionState);
			oDatabaseHelper.Dispose();	
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Update one new row into the database using the property Information
		/// </summary>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Update() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			oDatabaseHelper.AddParameter("@Key", _keyNonDefault );
			
			// Pass the value of '_firstName' as parameter 'FirstName' of the stored procedure.
			oDatabaseHelper.AddParameter("@FirstName", _firstNameNonDefault );
			
			// Pass the value of '_lastName' as parameter 'LastName' of the stored procedure.
			oDatabaseHelper.AddParameter("@LastName", _lastNameNonDefault );
			
			// Pass the value of '_username' as parameter 'Username' of the stored procedure.
			oDatabaseHelper.AddParameter("@Username", _usernameNonDefault );
			
			// Pass the value of '_password' as parameter 'Password' of the stored procedure.
			oDatabaseHelper.AddParameter("@Password", _passwordNonDefault );
			
			// Pass the value of '_email' as parameter 'Email' of the stored procedure.
			oDatabaseHelper.AddParameter("@Email", _emailNonDefault );
			
			// Pass the value of '_tStamp' as parameter 'TStamp' of the stored procedure.
			oDatabaseHelper.AddParameter("@TStamp", _tStampNonDefault );
			
			// Pass the value of '_dateCreated' as parameter 'DateCreated' of the stored procedure.
			oDatabaseHelper.AddParameter("@DateCreated", _dateCreatedNonDefault );
			
			// Pass the value of '_createdBy' as parameter 'CreatedBy' of the stored procedure.
			oDatabaseHelper.AddParameter("@CreatedBy", _createdByNonDefault );
			
			// Pass the value of '_lastUpdatedBy' as parameter 'LastUpdatedBy' of the stored procedure.
			oDatabaseHelper.AddParameter("@LastUpdatedBy", _lastUpdatedByNonDefault );
			
			// Pass the value of '_source' as parameter 'Source' of the stored procedure.
			oDatabaseHelper.AddParameter("@Source", _sourceNonDefault );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TUser_Update", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete one row from the database using the property Information
		/// </summary>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public bool Delete() 
		{
			bool ExecutionState = false;
			oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			
			// Pass the value of '_key' as parameter 'Key' of the stored procedure.
			if(_keyNonDefault!=null)
				oDatabaseHelper.AddParameter("@Key", _keyNonDefault );
			else
				oDatabaseHelper.AddParameter("@Key", DBNull.Value );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TUser_Delete", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete one row from the database using the primary key information
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool Delete(TUserPrimaryKey pk) 
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
   oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
   
			oDatabaseHelper.ExecuteScalar("gsp_TUser_Delete", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will Delete row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="TUserFields">Field of the class TUser</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		///
		/// <returns>True if succeeded</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static bool DeleteByField(string field, object fieldValue)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			oDatabaseHelper.ExecuteScalar("gsp_TUser_DeleteByField", ref ExecutionState);
			oDatabaseHelper.Dispose();
			return ExecutionState;
			
		}

		/// <summary>
		/// This method will return an object representing the record matching the primary key information specified.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOne(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectbyPrimaryKey", ref ExecutionState);
			if (dr.Read())
			{
				TUser obj=new TUser();	
				PopulateObjectFromReader(obj,dr);
				dr.Close();              
				oDatabaseHelper.Dispose();
				return obj;
			}
			else
			{
				dr.Close();
				oDatabaseHelper.Dispose();
				return null;
			}
			
		}

		/// <summary>
		/// This method will return a list of objects representing all records in the table.
		/// </summary>
		///
		/// <returns>list of objects of class TUser in the form of object of TUsers </returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUsers SelectAll()
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectAll", ref ExecutionState);
			TUsers TUsers = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TUsers;
			
		}

		/// <summary>
		/// Deprecated. Use SelectByField(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation) instead. This method will get row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TUser</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		///
		/// <returns>List of object of class TUser in the form of an object of class TUsers</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUsers SelectByField(string field, object fieldValue)
		{

			

			
			
			
			

			return SelectByField(field, fieldValue, null, TypeOperation.Equal);
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TUser</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		/// <param name="fieldValue2" type="object">Value for the field specified.</param>
		/// <param name="typeOperation" type="TypeOperation">Operator that is used if fieldValue2=null or fieldValue2="".</param>
		///
		/// <returns>List of object of class TUser in the form of an object of class TUsers</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUsers SelectByField(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			oDatabaseHelper.AddParameter("@Value2", fieldValue2 );
			oDatabaseHelper.AddParameter("@Operation", OperationCollection.Operation[(int)typeOperation] );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectByField", ref ExecutionState);
			TUsers TUsers = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TUsers;
			
		}

		/// <summary>
		/// This method will return a list of objects representing the specified number of entries from the specified record number in the table.
		/// </summary>
		///
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		/// <param name="orderByStatement" type="string">The field value to number</param>
		///
		/// <returns>list of objects of class TUser in the form of object of TUsers </returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUsers SelectAllPaged(int? pageSize, int? skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages);
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectAllPaged", ref ExecutionState);
			TUsers TUsers = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TUsers;
			
		}

		/// <summary>
		/// This method will return a list of objects representing the specified number of entries from the specified record number in the table 
		/// using the value of the field specified
		/// </summary>
		///
		/// <param name="field" type="string">Field of the class TUser</param>
		/// <param name="fieldValue" type="object">Value for the field specified.</param>
		/// <param name="fieldValue2" type="object">Value for the field specified.</param>
		/// <param name="typeOperation" type="TypeOperation">Operator that is used if fieldValue2=null or fieldValue2="".</param>
		/// <param name="orderByStatement" type="string">The field value to number.</param>
		/// <param name="pageSize" type="int">Number of records returned.</param>
		/// <param name="skipPages" type="int">The number of missing pages.</param>
		///
		/// <returns>List of object of class TUser in the form of an object of class TUsers</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUsers SelectByFieldPaged(string field, object fieldValue, object fieldValue2, TypeOperation typeOperation, int pageSize, int skipPages, string orderByStatement)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// Pass the specified field and its value to the stored procedure.
			oDatabaseHelper.AddParameter("@Field",field);
			oDatabaseHelper.AddParameter("@Value", fieldValue );
			oDatabaseHelper.AddParameter("@Value2", fieldValue2 );
			oDatabaseHelper.AddParameter("@Operation", OperationCollection.Operation[(int)typeOperation] );
			oDatabaseHelper.AddParameter("@PageSize",pageSize);
			oDatabaseHelper.AddParameter("@SkipPages", skipPages );
			oDatabaseHelper.AddParameter("@OrderByStatement", orderByStatement );
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectByFieldPaged", ref ExecutionState);
			TUsers TUsers = PopulateObjectsFromReader(dr);
			dr.Close();
			oDatabaseHelper.Dispose();
			return TUsers;
			
		}

		/// <summary>
		/// This method will return a count all records in the table.
		/// </summary>
		///
		/// <returns>count records</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static int SelectAllCount()
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();			
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr=oDatabaseHelper.ExecuteReader("gsp_TUser_SelectAllCount", ref ExecutionState);
			int count = 0;
            using (DataTable dt = new DataTable())
            {
                dt.Load(dr);
                count = Convert.ToInt32(dt.Rows[0][0]);
            }
			oDatabaseHelper.Dispose();
			return count;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOneWithTAppointmentUsingCounselorTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TUser obj=null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectOneWithTAppointmentUsingCounselorTUserKey", ref ExecutionState);
			if (dr.Read())
			{
				obj= new TUser();
				PopulateObjectFromReader(obj,dr);
				
				dr.NextResult();
				
				//Get the child records.
				obj.TAppointmentsUsingCounselorTUserKey=TAppointment.PopulateObjectsFromReader(dr);
			}
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOneWithTAppointmentUsingCanceledByTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TUser obj=null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectOneWithTAppointmentUsingCanceledByTUserKey", ref ExecutionState);
			if (dr.Read())
			{
				obj= new TUser();
				PopulateObjectFromReader(obj,dr);
				
				dr.NextResult();
				
				//Get the child records.
				obj.TAppointmentsUsingCanceledByTUserKey=TAppointment.PopulateObjectsFromReader(dr);
			}
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOneWithTAppointmentUsingScheduledByTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TUser obj=null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectOneWithTAppointmentUsingScheduledByTUserKey", ref ExecutionState);
			if (dr.Read())
			{
				obj= new TUser();
				PopulateObjectFromReader(obj,dr);
				
				dr.NextResult();
				
				//Get the child records.
				obj.TAppointmentsUsingScheduledByTUserKey=TAppointment.PopulateObjectsFromReader(dr);
			}
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOneWithTSchoolToTUserUsingTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TUser obj=null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectOneWithTSchoolToTUserUsingTUserKey", ref ExecutionState);
			if (dr.Read())
			{
				obj= new TUser();
				PopulateObjectFromReader(obj,dr);
				
				dr.NextResult();
				
				//Get the child records.
				obj.TSchoolToTUsersUsingTUserKey=TSchoolToTUser.PopulateObjectsFromReader(dr);
			}
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		/// <summary>
		/// This method will get row(s) from the database using the value of the field specified 
		/// along with the details of the child table.
		/// </summary>
		///
		/// <param name="pk" type="TUserPrimaryKey">Primary Key information based on which data is to be fetched.</param>
		///
		/// <returns>object of class TUser</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public static TUser SelectOneWithTStudentToCounselorUsingCounselorTUserKey(TUserPrimaryKey pk)
		{
			DatabaseHelper oDatabaseHelper = new DatabaseHelper();
            oDatabaseHelper.CommandTimeOut = CommandTimeOut;
			bool ExecutionState = false;
			TUser obj=null;
			
			// Pass the values of all key parameters to the stored procedure.
			System.Collections.Specialized.NameValueCollection nvc = pk.GetKeysAndValues();
			foreach (string nvcKey in nvc.Keys)
			{
				oDatabaseHelper.AddParameter("@" + nvcKey,nvc[nvcKey] );
			}
			
			// The parameter '@dlgErrorCode' will contain the status after execution of the stored procedure.
			oDatabaseHelper.AddParameter("@dlgErrorCode", -1, System.Data.ParameterDirection.Output);
			
			DbDataReader dr = oDatabaseHelper.ExecuteReader("gsp_TUser_SelectOneWithTStudentToCounselorUsingCounselorTUserKey", ref ExecutionState);
			if (dr.Read())
			{
				obj= new TUser();
				PopulateObjectFromReader(obj,dr);
				
				dr.NextResult();
				
				//Get the child records.
				obj.TStudentToCounselorsUsingCounselorTUserKey=TStudentToCounselor.PopulateObjectsFromReader(dr);
			}
			dr.Close();  
			oDatabaseHelper.Dispose();
			return obj;
			
		}

		#endregion	
		
		#region Methods (Private)

		/// <summary>
		/// tests a string to be a well formed xml or not,
		/// it throws ArgumentException when string text is not well formed.otherwise this 
		/// method is executed silently .
		/// </summary>
		/// <param name="text" type="string">xml string to validate.</param>
		/// <param name="fieldName" type="string">field Name to validate.</param>
		/// <exception cref="System.ArgumentException"> throws ArgumentException when text is not well formed parameter.otherwise this 
		/// method is executed silently .</exception>
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static void IsValidXmlString(string text,string fieldName)
		{
			XmlTextReader r = new XmlTextReader(new StringReader(text));
			try
			{
				while (r.Read())
				{
				  /*do nothing ,just continue as long as xml is valid*/ 
				}
			}
			catch(Exception)
			{
				throw new ArgumentException ("Field ("+fieldName+") xml text argument isn't well formed");				
			}
			finally
			{
				r.Close();
			
			}
		  //end silently(well formed xml)
		}    
		/// <summary>
		/// Populates the fields of a single objects from the columns found in an open reader.
		/// </summary>
		/// <param name="obj" type="TUser">Object of TUser to populate</param>
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static void PopulateObjectFromReader(TUserBase obj,IDataReader rdr) 
		{

			obj.Key = rdr.GetGuid(rdr.GetOrdinal(TUserFields.Key));
			obj.FirstName = rdr.GetString(rdr.GetOrdinal(TUserFields.FirstName));
			obj.LastName = rdr.GetString(rdr.GetOrdinal(TUserFields.LastName));
			obj.Username = rdr.GetString(rdr.GetOrdinal(TUserFields.Username));
			obj.Password = rdr.GetString(rdr.GetOrdinal(TUserFields.Password));
			obj.Email = rdr.GetString(rdr.GetOrdinal(TUserFields.Email));
			obj.TStamp = rdr.GetDateTime(rdr.GetOrdinal(TUserFields.TStamp));
			obj.DateCreated = rdr.GetDateTime(rdr.GetOrdinal(TUserFields.DateCreated));
			obj.CreatedBy = rdr.GetString(rdr.GetOrdinal(TUserFields.CreatedBy));
			if (!rdr.IsDBNull(rdr.GetOrdinal(TUserFields.LastUpdatedBy)))
			{
				obj.LastUpdatedBy = rdr.GetString(rdr.GetOrdinal(TUserFields.LastUpdatedBy));
			}
			
			obj.Source = rdr.GetString(rdr.GetOrdinal(TUserFields.Source));

		}

		/// <summary>
		/// Populates the fields for multiple objects from the columns found in an open reader.
		/// </summary>
		///
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <returns>Object of TUsers</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static TUsers PopulateObjectsFromReader(IDataReader rdr) 
		{
			TUsers list = new TUsers();
			
			while (rdr.Read())
			{
				TUser obj = new TUser();
				PopulateObjectFromReader(obj,rdr);
				list.Add(obj);
			}
			return list;
			
		}

		/// <summary>
		/// Populates the fields for multiple objects from the columns found in an open reader.
		/// </summary>
		///
		/// <param name="rdr" type="IDataReader">An object that implements the IDataReader interface</param>
		///
		/// <returns>Object of TUsers</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM		Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		internal static TUsers PopulateObjectsFromReaderWithCheckingReader(IDataReader rdr, DatabaseHelper oDatabaseHelper) 
		{

			TUsers list = new TUsers();
			
            if (rdr.Read())
			{
				TUser obj = new TUser();
				PopulateObjectFromReader(obj, rdr);
				list.Add(obj);
				while (rdr.Read())
				{
					obj = new TUser();
					PopulateObjectFromReader(obj, rdr);
					list.Add(obj);
				}
				oDatabaseHelper.Dispose();
				return list;
			}
			else
			{
				oDatabaseHelper.Dispose();
				return list;
			}
			
		}

	
	#endregion

	}
}

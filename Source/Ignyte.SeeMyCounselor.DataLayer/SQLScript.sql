-- Developer's comment header
-- TAppointment.sql
-- 
-- history:   11/9/2015 3:46:05 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TAppointment_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TAppointment] DROP CONSTRAINT [TAppointment_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TAppointment'))
  BEGIN
      ALTER TABLE [dbo].[TAppointment] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@ScheduledByTUserKey uniqueidentifier  
		@TAppointmentTypeKey uniqueidentifier  
		@CanceledByTUserKey uniqueidentifier = null  
		@TCancelReasonKey uniqueidentifier = null  
		@DateTimeScheduledFrom datetime  
		@DateTimeScheduledTo datetime  
		@IsStudentCheckedIn bit  
		@IsCounselorVerified bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointment' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@ScheduledByTUserKey uniqueidentifier , 
@TAppointmentTypeKey uniqueidentifier , 
@CanceledByTUserKey uniqueidentifier = null , 
@TCancelReasonKey uniqueidentifier = null , 
@DateTimeScheduledFrom datetime , 
@DateTimeScheduledTo datetime , 
@IsStudentCheckedIn bit , 
@IsCounselorVerified bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TAppointment'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CounselorTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @ScheduledByTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ScheduledByTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @ScheduledByTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ScheduledByTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TAppointmentTypeKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TAppointmentTypeKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TAppointmentTypeKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TAppointmentTypeKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CanceledByTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanceledByTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CanceledByTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanceledByTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TCancelReasonKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TCancelReasonKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TCancelReasonKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TCancelReasonKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeScheduledFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeScheduledFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeScheduledFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeScheduledTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeScheduledTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeScheduledTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsStudentCheckedIn is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsStudentCheckedIn')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsStudentCheckedIn =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsCounselorVerified is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsCounselorVerified')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsCounselorVerified =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TAppointment]( [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@CounselorTUserKey,@TStudentKey,@ScheduledByTUserKey,@TAppointmentTypeKey,@CanceledByTUserKey,@TCancelReasonKey,@DateTimeScheduledFrom,@DateTimeScheduledTo,@IsStudentCheckedIn,@IsCounselorVerified,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TAppointment_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@CounselorTUserKey uniqueidentifier = null  
		@TStudentKey uniqueidentifier = null  
		@ScheduledByTUserKey uniqueidentifier = null  
		@TAppointmentTypeKey uniqueidentifier = null  
		@CanceledByTUserKey uniqueidentifier = null  
		@TCancelReasonKey uniqueidentifier = null  
		@DateTimeScheduledFrom datetime = null  
		@DateTimeScheduledTo datetime = null  
		@IsStudentCheckedIn bit = null  
		@IsCounselorVerified bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointment' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@CounselorTUserKey uniqueidentifier = null , 
@TStudentKey uniqueidentifier = null , 
@ScheduledByTUserKey uniqueidentifier = null , 
@TAppointmentTypeKey uniqueidentifier = null , 
@CanceledByTUserKey uniqueidentifier = null , 
@TCancelReasonKey uniqueidentifier = null , 
@DateTimeScheduledFrom datetime = null , 
@DateTimeScheduledTo datetime = null , 
@IsStudentCheckedIn bit = null , 
@IsCounselorVerified bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TAppointment'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CounselorTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @ScheduledByTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ScheduledByTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @ScheduledByTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ScheduledByTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TAppointmentTypeKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TAppointmentTypeKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TAppointmentTypeKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TAppointmentTypeKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CanceledByTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanceledByTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CanceledByTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanceledByTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TCancelReasonKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TCancelReasonKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TCancelReasonKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TCancelReasonKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeScheduledFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeScheduledFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeScheduledFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeScheduledTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeScheduledTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeScheduledTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsStudentCheckedIn is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsStudentCheckedIn')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsStudentCheckedIn =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsCounselorVerified is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsCounselorVerified')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsCounselorVerified =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolKey], [CounselorTUserKey], [TStudentKey], [ScheduledByTUserKey], [TAppointmentTypeKey], [CanceledByTUserKey], [TCancelReasonKey], [DateTimeScheduledFrom], [DateTimeScheduledTo], [IsStudentCheckedIn], [IsCounselorVerified], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TAppointment] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TAppointment]( [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolKey], INSERTED.[CounselorTUserKey], INSERTED.[TStudentKey], INSERTED.[ScheduledByTUserKey], INSERTED.[TAppointmentTypeKey], INSERTED.[CanceledByTUserKey], INSERTED.[TCancelReasonKey], INSERTED.[DateTimeScheduledFrom], INSERTED.[DateTimeScheduledTo], INSERTED.[IsStudentCheckedIn], INSERTED.[IsCounselorVerified], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolKey], [CounselorTUserKey], [TStudentKey], [ScheduledByTUserKey], [TAppointmentTypeKey], [CanceledByTUserKey], [TCancelReasonKey], [DateTimeScheduledFrom], [DateTimeScheduledTo], [IsStudentCheckedIn], [IsCounselorVerified], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolKey,@CounselorTUserKey,@TStudentKey,@ScheduledByTUserKey,@TAppointmentTypeKey,@CanceledByTUserKey,@TCancelReasonKey,@DateTimeScheduledFrom,@DateTimeScheduledTo,@IsStudentCheckedIn,@IsCounselorVerified,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolKey], [CounselorTUserKey], [TStudentKey], [ScheduledByTUserKey], [TAppointmentTypeKey], [CanceledByTUserKey], [TCancelReasonKey], [DateTimeScheduledFrom], [DateTimeScheduledTo], [IsStudentCheckedIn], [IsCounselorVerified], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@ScheduledByTUserKey uniqueidentifier  
		@TAppointmentTypeKey uniqueidentifier  
		@CanceledByTUserKey uniqueidentifier = null  
		@TCancelReasonKey uniqueidentifier = null  
		@DateTimeScheduledFrom datetime  
		@DateTimeScheduledTo datetime  
		@IsStudentCheckedIn bit  
		@IsCounselorVerified bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointment' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_Insert]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@ScheduledByTUserKey uniqueidentifier , 
@TAppointmentTypeKey uniqueidentifier , 
@CanceledByTUserKey uniqueidentifier = null , 
@TCancelReasonKey uniqueidentifier = null , 
@DateTimeScheduledFrom datetime , 
@DateTimeScheduledTo datetime , 
@IsStudentCheckedIn bit , 
@IsCounselorVerified bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TAppointment]( [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@CounselorTUserKey,@TStudentKey,@ScheduledByTUserKey,@TAppointmentTypeKey,@CanceledByTUserKey,@TCancelReasonKey,@DateTimeScheduledFrom,@DateTimeScheduledTo,@IsStudentCheckedIn,@IsCounselorVerified,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@ScheduledByTUserKey uniqueidentifier  
		@TAppointmentTypeKey uniqueidentifier  
		@CanceledByTUserKey uniqueidentifier = null  
		@TCancelReasonKey uniqueidentifier = null  
		@DateTimeScheduledFrom datetime  
		@DateTimeScheduledTo datetime  
		@IsStudentCheckedIn bit  
		@IsCounselorVerified bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TAppointment' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_Update]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@ScheduledByTUserKey uniqueidentifier , 
@TAppointmentTypeKey uniqueidentifier , 
@CanceledByTUserKey uniqueidentifier = null , 
@TCancelReasonKey uniqueidentifier = null , 
@DateTimeScheduledFrom datetime , 
@DateTimeScheduledTo datetime , 
@IsStudentCheckedIn bit , 
@IsCounselorVerified bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TAppointment]
SET
	[TSchoolKey] = @TSchoolKey ,
	[CounselorTUserKey] = @CounselorTUserKey ,
	[TStudentKey] = @TStudentKey ,
	[ScheduledByTUserKey] = @ScheduledByTUserKey ,
	[TAppointmentTypeKey] = @TAppointmentTypeKey ,
	[CanceledByTUserKey] = @CanceledByTUserKey ,
	[TCancelReasonKey] = @TCancelReasonKey ,
	[DateTimeScheduledFrom] = @DateTimeScheduledFrom ,
	[DateTimeScheduledTo] = @DateTimeScheduledTo ,
	[IsStudentCheckedIn] = @IsStudentCheckedIn ,
	[IsCounselorVerified] = @IsCounselorVerified ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TAppointment' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TAppointment]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TAppointment] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TAppointment]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TAppointment_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TAppointment]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TAppointment] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TAppointment] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TAppointment' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TAppointment] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TAppointment] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TAppointment]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[TAppointmentTypeKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[TCancelReasonKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[CounselorTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[CanceledByTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointment]
WHERE
	[ScheduledByTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [TAppointmentTypeKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [TCancelReasonKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyTStudentKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyTStudentKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [TStudentKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [CounselorTUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [CanceledByTUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[CounselorTUserKey],[TStudentKey],[ScheduledByTUserKey],[TAppointmentTypeKey],[CanceledByTUserKey],[TCancelReasonKey],[DateTimeScheduledFrom],[DateTimeScheduledTo],[IsStudentCheckedIn],[IsCounselorVerified],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointment]
			WHERE [ScheduledByTUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyTAppointmentTypeKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTAppointmentTypeKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyTAppointmentTypeKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTAppointmentTypeKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[TAppointmentTypeKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyTCancelReasonKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTCancelReasonKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyTCancelReasonKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTCancelReasonKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[TCancelReasonKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[CounselorTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyCanceledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyCanceledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyCanceledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyCanceledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[CanceledByTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointment_DeleteAllByForeignKeyScheduledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointment_DeleteAllByForeignKeyScheduledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointment_DeleteAllByForeignKeyScheduledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointment' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointment_DeleteAllByForeignKeyScheduledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointment]
WHERE
	[ScheduledByTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TAppointmentType.sql
-- 
-- history:   11/9/2015 3:46:05 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TAppointmentType_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TAppointmentType] DROP CONSTRAINT [TAppointmentType_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TAppointmentType'))
  BEGIN
      ALTER TABLE [dbo].[TAppointmentType] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TSupportedColorKey uniqueidentifier  
		@Description varchar (300)  
		@LengthOfTimeInMinutes int  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointmentType' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TSupportedColorKey uniqueidentifier , 
@Description varchar (300) , 
@LengthOfTimeInMinutes int , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TAppointmentType'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSupportedColorKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSupportedColorKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSupportedColorKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSupportedColorKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Description is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Description')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Description =  convert ( varchar (300),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LengthOfTimeInMinutes is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LengthOfTimeInMinutes')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LengthOfTimeInMinutes =  convert ( int,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TAppointmentType]( [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@TSupportedColorKey,@Description,@LengthOfTimeInMinutes,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TAppointmentType_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@TSupportedColorKey uniqueidentifier = null  
		@Description varchar (300) = null  
		@LengthOfTimeInMinutes int = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointmentType' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@TSupportedColorKey uniqueidentifier = null , 
@Description varchar (300) = null , 
@LengthOfTimeInMinutes int = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TAppointmentType'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSupportedColorKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSupportedColorKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSupportedColorKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSupportedColorKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Description is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Description')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Description =  convert ( varchar (300),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LengthOfTimeInMinutes is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LengthOfTimeInMinutes')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LengthOfTimeInMinutes =  convert ( int,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolKey], [TSupportedColorKey], [Description], [LengthOfTimeInMinutes], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TAppointmentType] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TAppointmentType]( [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolKey], INSERTED.[TSupportedColorKey], INSERTED.[Description], INSERTED.[LengthOfTimeInMinutes], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolKey], [TSupportedColorKey], [Description], [LengthOfTimeInMinutes], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolKey,@TSupportedColorKey,@Description,@LengthOfTimeInMinutes,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolKey], [TSupportedColorKey], [Description], [LengthOfTimeInMinutes], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TSupportedColorKey uniqueidentifier  
		@Description varchar (300)  
		@LengthOfTimeInMinutes int  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TAppointmentType' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_Insert]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TSupportedColorKey uniqueidentifier , 
@Description varchar (300) , 
@LengthOfTimeInMinutes int , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TAppointmentType]( [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@TSupportedColorKey,@Description,@LengthOfTimeInMinutes,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TSupportedColorKey uniqueidentifier  
		@Description varchar (300)  
		@LengthOfTimeInMinutes int  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TAppointmentType' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_Update]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TSupportedColorKey uniqueidentifier , 
@Description varchar (300) , 
@LengthOfTimeInMinutes int , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TAppointmentType]
SET
	[TSchoolKey] = @TSchoolKey ,
	[TSupportedColorKey] = @TSupportedColorKey ,
	[Description] = @Description ,
	[LengthOfTimeInMinutes] = @LengthOfTimeInMinutes ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TAppointmentType' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TAppointmentType]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TAppointmentType'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TAppointmentType] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointmentType]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TAppointmentType]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TAppointmentType'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TAppointmentType_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TAppointmentType]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TAppointmentType] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TAppointmentType] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TAppointmentType' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TAppointmentType] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TAppointmentType] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TAppointmentType]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectOneWithTAppointmentUsingTAppointmentTypeKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectOneWithTAppointmentUsingTAppointmentTypeKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectOneWithTAppointmentUsingTAppointmentTypeKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectOneWithTAppointmentUsingTAppointmentTypeKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TAppointmentType_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyTAppointmentTypeKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointmentType]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TAppointmentType]
WHERE
	[TSupportedColorKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointmentType]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[TSupportedColorKey],[Description],[LengthOfTimeInMinutes],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TAppointmentType]
			WHERE [TSupportedColorKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointmentType]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSupportedColorKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSupportedColorKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TAppointmentType_DeleteAllByForeignKeyTSupportedColorKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TAppointmentType' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TAppointmentType_DeleteAllByForeignKeyTSupportedColorKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TAppointmentType]
WHERE
	[TSupportedColorKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TCancelReason.sql
-- 
-- history:   11/9/2015 3:46:05 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TCancelReason_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TCancelReason] DROP CONSTRAINT [TCancelReason_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TCancelReason'))
  BEGIN
      ALTER TABLE [dbo].[TCancelReason] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@Description varchar (300)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCancelReason' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@Description varchar (300) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TCancelReason'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Description is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Description')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Description =  convert ( varchar (300),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TCancelReason]( [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@Description,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TCancelReason_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@Description varchar (300) = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCancelReason' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@Description varchar (300) = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TCancelReason'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Description is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Description')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Description =  convert ( varchar (300),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolKey], [Description], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TCancelReason] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TCancelReason]( [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolKey], INSERTED.[Description], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolKey], [Description], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolKey,@Description,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolKey], [Description], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@Description varchar (300)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCancelReason' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_Insert]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@Description varchar (300) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TCancelReason]( [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@Description,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@Description varchar (300)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TCancelReason' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_Update]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@Description varchar (300) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TCancelReason]
SET
	[TSchoolKey] = @TSchoolKey ,
	[Description] = @Description ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TCancelReason' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TCancelReason]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TCancelReason'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TCancelReason] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TCancelReason]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TCancelReason]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TCancelReason'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TCancelReason_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TCancelReason]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TCancelReason] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TCancelReason] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TCancelReason' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TCancelReason] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TCancelReason] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TCancelReason]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectOneWithTAppointmentUsingTCancelReasonKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectOneWithTAppointmentUsingTCancelReasonKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectOneWithTAppointmentUsingTCancelReasonKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCancelReason' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectOneWithTAppointmentUsingTCancelReasonKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TCancelReason_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyTCancelReasonKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TCancelReason]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[Description],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TCancelReason]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCancelReason_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCancelReason_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCancelReason_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCancelReason' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCancelReason_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TCancelReason]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TCounselorBusyTime.sql
-- 
-- history:   11/9/2015 3:46:05 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TCounselorBusyTime_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TCounselorBusyTime] DROP CONSTRAINT [TCounselorBusyTime_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TCounselorBusyTime'))
  BEGIN
      ALTER TABLE [dbo].[TCounselorBusyTime] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolToTUserKey uniqueidentifier  
		@BusyDateTimeFrom datetime  
		@BusyDatTimeeTo datetime  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCounselorBusyTime' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolToTUserKey uniqueidentifier , 
@BusyDateTimeFrom datetime , 
@BusyDatTimeeTo datetime , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TCounselorBusyTime'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolToTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolToTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolToTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolToTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @BusyDateTimeFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='BusyDateTimeFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @BusyDateTimeFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @BusyDatTimeeTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='BusyDatTimeeTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @BusyDatTimeeTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TCounselorBusyTime]( [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolToTUserKey,@BusyDateTimeFrom,@BusyDatTimeeTo,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TCounselorBusyTime_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolToTUserKey uniqueidentifier = null  
		@BusyDateTimeFrom datetime = null  
		@BusyDatTimeeTo datetime = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCounselorBusyTime' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolToTUserKey uniqueidentifier = null , 
@BusyDateTimeFrom datetime = null , 
@BusyDatTimeeTo datetime = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TCounselorBusyTime'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolToTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolToTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolToTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolToTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @BusyDateTimeFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='BusyDateTimeFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @BusyDateTimeFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @BusyDatTimeeTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='BusyDatTimeeTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @BusyDatTimeeTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolToTUserKey], [BusyDateTimeFrom], [BusyDatTimeeTo], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TCounselorBusyTime] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TCounselorBusyTime]( [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolToTUserKey], INSERTED.[BusyDateTimeFrom], INSERTED.[BusyDatTimeeTo], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolToTUserKey], [BusyDateTimeFrom], [BusyDatTimeeTo], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolToTUserKey,@BusyDateTimeFrom,@BusyDatTimeeTo,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolToTUserKey], [BusyDateTimeFrom], [BusyDatTimeeTo], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolToTUserKey uniqueidentifier  
		@BusyDateTimeFrom datetime  
		@BusyDatTimeeTo datetime  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TCounselorBusyTime' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_Insert]
@Key uniqueidentifier , 
@TSchoolToTUserKey uniqueidentifier , 
@BusyDateTimeFrom datetime , 
@BusyDatTimeeTo datetime , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TCounselorBusyTime]( [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolToTUserKey,@BusyDateTimeFrom,@BusyDatTimeeTo,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolToTUserKey uniqueidentifier  
		@BusyDateTimeFrom datetime  
		@BusyDatTimeeTo datetime  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TCounselorBusyTime' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_Update]
@Key uniqueidentifier , 
@TSchoolToTUserKey uniqueidentifier , 
@BusyDateTimeFrom datetime , 
@BusyDatTimeeTo datetime , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TCounselorBusyTime]
SET
	[TSchoolToTUserKey] = @TSchoolToTUserKey ,
	[BusyDateTimeFrom] = @BusyDateTimeFrom ,
	[BusyDatTimeeTo] = @BusyDatTimeeTo ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TCounselorBusyTime' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TCounselorBusyTime]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TCounselorBusyTime'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TCounselorBusyTime] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TCounselorBusyTime]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TCounselorBusyTime]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TCounselorBusyTime'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TCounselorBusyTime_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TCounselorBusyTime]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TCounselorBusyTime] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TCounselorBusyTime] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TCounselorBusyTime' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TCounselorBusyTime] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TCounselorBusyTime] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TCounselorBusyTime]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TCounselorBusyTime]
WHERE
	[TSchoolToTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolToTUserKey],[BusyDateTimeFrom],[BusyDatTimeeTo],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TCounselorBusyTime]
			WHERE [TSchoolToTUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TCounselorBusyTime_DeleteAllByForeignKeyTSchoolToTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TCounselorBusyTime_DeleteAllByForeignKeyTSchoolToTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TCounselorBusyTime_DeleteAllByForeignKeyTSchoolToTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TCounselorBusyTime' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TCounselorBusyTime_DeleteAllByForeignKeyTSchoolToTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TCounselorBusyTime]
WHERE
	[TSchoolToTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TRequest.sql
-- 
-- history:   11/9/2015 3:46:05 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TRequest_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TRequest] DROP CONSTRAINT [TRequest_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TRequest'))
  BEGIN
      ALTER TABLE [dbo].[TRequest] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@DateTimeRequestedFrom datetime  
		@DateTimeRequestedTo datetime  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRequest' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@DateTimeRequestedFrom datetime , 
@DateTimeRequestedTo datetime , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TRequest'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeRequestedFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeRequestedFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeRequestedFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeRequestedTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeRequestedTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeRequestedTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TRequest]( [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TStudentKey,@TSchoolKey,@DateTimeRequestedFrom,@DateTimeRequestedTo,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TRequest_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TStudentKey uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@DateTimeRequestedFrom datetime = null  
		@DateTimeRequestedTo datetime = null  
		@IsActive bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRequest' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TRequest_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TStudentKey uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@DateTimeRequestedFrom datetime = null , 
@DateTimeRequestedTo datetime = null , 
@IsActive bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TRequest'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeRequestedFrom is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeRequestedFrom')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeRequestedFrom =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateTimeRequestedTo is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateTimeRequestedTo')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateTimeRequestedTo =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TStudentKey], [TSchoolKey], [DateTimeRequestedFrom], [DateTimeRequestedTo], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TRequest] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TRequest]( [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TStudentKey], INSERTED.[TSchoolKey], INSERTED.[DateTimeRequestedFrom], INSERTED.[DateTimeRequestedTo], INSERTED.[IsActive], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TStudentKey], [TSchoolKey], [DateTimeRequestedFrom], [DateTimeRequestedTo], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TStudentKey,@TSchoolKey,@DateTimeRequestedFrom,@DateTimeRequestedTo,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TStudentKey], [TSchoolKey], [DateTimeRequestedFrom], [DateTimeRequestedTo], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@DateTimeRequestedFrom datetime  
		@DateTimeRequestedTo datetime  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRequest' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_Insert]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@DateTimeRequestedFrom datetime , 
@DateTimeRequestedTo datetime , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TRequest]( [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TStudentKey,@TSchoolKey,@DateTimeRequestedFrom,@DateTimeRequestedTo,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@DateTimeRequestedFrom datetime  
		@DateTimeRequestedTo datetime  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TRequest' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_Update]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@DateTimeRequestedFrom datetime , 
@DateTimeRequestedTo datetime , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TRequest]
SET
	[TStudentKey] = @TStudentKey ,
	[TSchoolKey] = @TSchoolKey ,
	[DateTimeRequestedFrom] = @DateTimeRequestedFrom ,
	[DateTimeRequestedTo] = @DateTimeRequestedTo ,
	[IsActive] = @IsActive ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TRequest' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TRequest]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TRequest'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TRequest] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TRequest]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TRequest]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TRequest'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TRequest_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TRequest]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TRequest] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TRequest] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TRequest' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TRequest] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TRequest] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TRequest]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TRequest]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TRequest]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TRequest]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_SelectAllByForeignKeyTStudentKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_SelectAllByForeignKeyTStudentKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TStudentKey],[TSchoolKey],[DateTimeRequestedFrom],[DateTimeRequestedTo],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TRequest]
			WHERE [TStudentKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TRequest]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRequest_DeleteAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRequest_DeleteAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRequest_DeleteAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:05 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRequest' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRequest_DeleteAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TRequest]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TRole.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TRole_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TRole] DROP CONSTRAINT [TRole_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TRole'))
  BEGIN
      ALTER TABLE [dbo].[TRole] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (50)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRole' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@Name varchar (50) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TRole'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TRole]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TRole_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@Name varchar (50) = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRole' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TRole_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@Name varchar (50) = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TRole'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TRole] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TRole]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[Name], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (50)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TRole' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_Insert]
@Key uniqueidentifier , 
@Name varchar (50) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TRole]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (50)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TRole' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_Update]
@Key uniqueidentifier , 
@Name varchar (50) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TRole]
SET
	[Name] = @Name ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TRole' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TRole]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TRole'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TRole] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRole' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TRole]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TRole' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TRole]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TRole'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TRole_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TRole]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TRole' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TRole] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TRole] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TRole' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TRole] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TRole] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TRole' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TRole]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TRole_SelectOneWithTSchoolToTUserUsingTRoleKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TRole_SelectOneWithTSchoolToTUserUsingTRoleKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TRole_SelectOneWithTSchoolToTUserUsingTRoleKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TRole' and also the respective child records from 'TSchoolToTUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TRole_SelectOneWithTSchoolToTUserUsingTRoleKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TRole_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				





-- Developer's comment header
-- TSchool.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TSchool_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TSchool] DROP CONSTRAINT [TSchool_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TSchool'))
  BEGIN
      ALTER TABLE [dbo].[TSchool] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (100)  
		@AddressLine1 varchar (100)  
		@AddressLine2 varchar (100)  
		@City varchar (50)  
		@StateOrProvince varchar (50)  
		@ZipOrPostalCode varchar (20)  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchool' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@Name varchar (100) , 
@AddressLine1 varchar (100) , 
@AddressLine2 varchar (100) , 
@City varchar (50) , 
@StateOrProvince varchar (50) , 
@ZipOrPostalCode varchar (20) , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TSchool'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @AddressLine1 is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='AddressLine1')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @AddressLine1 =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @AddressLine2 is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='AddressLine2')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @AddressLine2 =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @City is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='City')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @City =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @StateOrProvince is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='StateOrProvince')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @StateOrProvince =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @ZipOrPostalCode is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ZipOrPostalCode')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ZipOrPostalCode =  convert ( varchar (20),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSchool]( [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@AddressLine1,@AddressLine2,@City,@StateOrProvince,@ZipOrPostalCode,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TSchool_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@Name varchar (100) = null  
		@AddressLine1 varchar (100) = null  
		@AddressLine2 varchar (100) = null  
		@City varchar (50) = null  
		@StateOrProvince varchar (50) = null  
		@ZipOrPostalCode varchar (20) = null  
		@IsActive bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchool' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TSchool_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@Name varchar (100) = null , 
@AddressLine1 varchar (100) = null , 
@AddressLine2 varchar (100) = null , 
@City varchar (50) = null , 
@StateOrProvince varchar (50) = null , 
@ZipOrPostalCode varchar (20) = null , 
@IsActive bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TSchool'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @AddressLine1 is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='AddressLine1')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @AddressLine1 =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @AddressLine2 is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='AddressLine2')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @AddressLine2 =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @City is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='City')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @City =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @StateOrProvince is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='StateOrProvince')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @StateOrProvince =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @ZipOrPostalCode is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ZipOrPostalCode')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ZipOrPostalCode =  convert ( varchar (20),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [Name], [AddressLine1], [AddressLine2], [City], [StateOrProvince], [ZipOrPostalCode], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TSchool] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TSchool]( [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[Name], INSERTED.[AddressLine1], INSERTED.[AddressLine2], INSERTED.[City], INSERTED.[StateOrProvince], INSERTED.[ZipOrPostalCode], INSERTED.[IsActive], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [Name], [AddressLine1], [AddressLine2], [City], [StateOrProvince], [ZipOrPostalCode], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@Name,@AddressLine1,@AddressLine2,@City,@StateOrProvince,@ZipOrPostalCode,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [Name], [AddressLine1], [AddressLine2], [City], [StateOrProvince], [ZipOrPostalCode], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (100)  
		@AddressLine1 varchar (100)  
		@AddressLine2 varchar (100)  
		@City varchar (50)  
		@StateOrProvince varchar (50)  
		@ZipOrPostalCode varchar (20)  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchool' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_Insert]
@Key uniqueidentifier , 
@Name varchar (100) , 
@AddressLine1 varchar (100) , 
@AddressLine2 varchar (100) , 
@City varchar (50) , 
@StateOrProvince varchar (50) , 
@ZipOrPostalCode varchar (20) , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSchool]( [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@AddressLine1,@AddressLine2,@City,@StateOrProvince,@ZipOrPostalCode,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (100)  
		@AddressLine1 varchar (100)  
		@AddressLine2 varchar (100)  
		@City varchar (50)  
		@StateOrProvince varchar (50)  
		@ZipOrPostalCode varchar (20)  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TSchool' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_Update]
@Key uniqueidentifier , 
@Name varchar (100) , 
@AddressLine1 varchar (100) , 
@AddressLine2 varchar (100) , 
@City varchar (50) , 
@StateOrProvince varchar (50) , 
@ZipOrPostalCode varchar (20) , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TSchool]
SET
	[Name] = @Name ,
	[AddressLine1] = @AddressLine1 ,
	[AddressLine2] = @AddressLine2 ,
	[City] = @City ,
	[StateOrProvince] = @StateOrProvince ,
	[ZipOrPostalCode] = @ZipOrPostalCode ,
	[IsActive] = @IsActive ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TSchool' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TSchool]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TSchool'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TSchool] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSchool]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TSchool' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TSchool]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TSchool'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TSchool_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TSchool]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TSchool' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSchool] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSchool] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TSchool' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TSchool] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[Name],[AddressLine1],[AddressLine2],[City],[StateOrProvince],[ZipOrPostalCode],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TSchool] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TSchool' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TSchool]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTAppointmentUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTAppointmentUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTAppointmentUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTAppointmentUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTAppointmentTypeUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTAppointmentTypeUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTAppointmentTypeUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TAppointmentType'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTAppointmentTypeUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointmentType_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTCancelReasonUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTCancelReasonUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTCancelReasonUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TCancelReason'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTCancelReasonUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TCancelReason_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTRequestUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTRequestUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTRequestUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TRequest'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTRequestUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TRequest_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTSchoolToTUserUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTSchoolToTUserUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTSchoolToTUserUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TSchoolToTUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTSchoolToTUserUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchool_SelectOneWithTSettingUsingTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchool_SelectOneWithTSettingUsingTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchool_SelectOneWithTSettingUsingTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchool' and also the respective child records from 'TSetting'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchool_SelectOneWithTSettingUsingTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchool_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TSetting_SelectAllByForeignKeyTSchoolKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				





-- Developer's comment header
-- TSchoolToTUser.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TSchoolToTUser_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TSchoolToTUser] DROP CONSTRAINT [TSchoolToTUser_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TSchoolToTUser'))
  BEGIN
      ALTER TABLE [dbo].[TSchoolToTUser] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TUserKey uniqueidentifier  
		@TRoleKey uniqueidentifier  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchoolToTUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TUserKey uniqueidentifier , 
@TRoleKey uniqueidentifier , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TSchoolToTUser'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TRoleKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TRoleKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TRoleKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TRoleKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSchoolToTUser]( [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@TUserKey,@TRoleKey,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TSchoolToTUser_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@TUserKey uniqueidentifier = null  
		@TRoleKey uniqueidentifier = null  
		@IsActive bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchoolToTUser' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@TUserKey uniqueidentifier = null , 
@TRoleKey uniqueidentifier = null , 
@IsActive bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TSchoolToTUser'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TRoleKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TRoleKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TRoleKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TRoleKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @IsActive is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsActive')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsActive =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolKey], [TUserKey], [TRoleKey], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TSchoolToTUser] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TSchoolToTUser]( [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolKey], INSERTED.[TUserKey], INSERTED.[TRoleKey], INSERTED.[IsActive], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolKey], [TUserKey], [TRoleKey], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolKey,@TUserKey,@TRoleKey,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolKey], [TUserKey], [TRoleKey], [IsActive], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TUserKey uniqueidentifier  
		@TRoleKey uniqueidentifier  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSchoolToTUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_Insert]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TUserKey uniqueidentifier , 
@TRoleKey uniqueidentifier , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSchoolToTUser]( [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@TUserKey,@TRoleKey,@IsActive,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@TUserKey uniqueidentifier  
		@TRoleKey uniqueidentifier  
		@IsActive bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TSchoolToTUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_Update]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@TUserKey uniqueidentifier , 
@TRoleKey uniqueidentifier , 
@IsActive bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TSchoolToTUser]
SET
	[TSchoolKey] = @TSchoolKey ,
	[TUserKey] = @TUserKey ,
	[TRoleKey] = @TRoleKey ,
	[IsActive] = @IsActive ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TSchoolToTUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TSchoolToTUser]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TSchoolToTUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TSchoolToTUser] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSchoolToTUser]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TSchoolToTUser]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TSchoolToTUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TSchoolToTUser_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TSchoolToTUser]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSchoolToTUser] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSchoolToTUser] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TSchoolToTUser' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TSchoolToTUser] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TSchoolToTUser] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TSchoolToTUser]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectOneWithTCounselorBusyTimeUsingTSchoolToTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectOneWithTCounselorBusyTimeUsingTSchoolToTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectOneWithTCounselorBusyTimeUsingTSchoolToTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' and also the respective child records from 'TCounselorBusyTime'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectOneWithTCounselorBusyTimeUsingTSchoolToTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSchoolToTUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TCounselorBusyTime_SelectAllByForeignKeyTSchoolToTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TRoleKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTRoleKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TSchoolToTUser]
			WHERE [TRoleKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TSchoolToTUser]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[TUserKey],[TRoleKey],[IsActive],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TSchoolToTUser]
			WHERE [TUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTRoleKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTRoleKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_DeleteAllByForeignKeyTRoleKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTRoleKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TRoleKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSchoolToTUser_DeleteAllByForeignKeyTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSchoolToTUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSchoolToTUser_DeleteAllByForeignKeyTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TSchoolToTUser]
WHERE
	[TUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TSetting.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TSetting_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TSetting] DROP CONSTRAINT [TSetting_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TSetting'))
  BEGIN
      ALTER TABLE [dbo].[TSetting] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@ShouldVerifyCheckins bit  
		@CounselorStartTime datetime  
		@CounselorEndTime datetime  
		@CanCounselorAcceptRequest bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSetting' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@ShouldVerifyCheckins bit , 
@CounselorStartTime datetime , 
@CounselorEndTime datetime , 
@CanCounselorAcceptRequest bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TSetting'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @ShouldVerifyCheckins is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ShouldVerifyCheckins')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ShouldVerifyCheckins =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorStartTime is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorStartTime')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorStartTime =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorEndTime is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorEndTime')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorEndTime =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanCounselorAcceptRequest is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanCounselorAcceptRequest')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanCounselorAcceptRequest =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSetting]( [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@ShouldVerifyCheckins,@CounselorStartTime,@CounselorEndTime,@CanCounselorAcceptRequest,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TSetting_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TSchoolKey uniqueidentifier = null  
		@ShouldVerifyCheckins bit = null  
		@CounselorStartTime datetime = null  
		@CounselorEndTime datetime = null  
		@CanCounselorAcceptRequest bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSetting' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TSetting_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TSchoolKey uniqueidentifier = null , 
@ShouldVerifyCheckins bit = null , 
@CounselorStartTime datetime = null , 
@CounselorEndTime datetime = null , 
@CanCounselorAcceptRequest bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TSetting'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TSchoolKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TSchoolKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TSchoolKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TSchoolKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @ShouldVerifyCheckins is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='ShouldVerifyCheckins')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @ShouldVerifyCheckins =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorStartTime is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorStartTime')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorStartTime =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorEndTime is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorEndTime')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorEndTime =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanCounselorAcceptRequest is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanCounselorAcceptRequest')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanCounselorAcceptRequest =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TSchoolKey], [ShouldVerifyCheckins], [CounselorStartTime], [CounselorEndTime], [CanCounselorAcceptRequest], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TSetting] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TSetting]( [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TSchoolKey], INSERTED.[ShouldVerifyCheckins], INSERTED.[CounselorStartTime], INSERTED.[CounselorEndTime], INSERTED.[CanCounselorAcceptRequest], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TSchoolKey], [ShouldVerifyCheckins], [CounselorStartTime], [CounselorEndTime], [CanCounselorAcceptRequest], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TSchoolKey,@ShouldVerifyCheckins,@CounselorStartTime,@CounselorEndTime,@CanCounselorAcceptRequest,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TSchoolKey], [ShouldVerifyCheckins], [CounselorStartTime], [CounselorEndTime], [CanCounselorAcceptRequest], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@ShouldVerifyCheckins bit  
		@CounselorStartTime datetime  
		@CounselorEndTime datetime  
		@CanCounselorAcceptRequest bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSetting' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_Insert]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@ShouldVerifyCheckins bit , 
@CounselorStartTime datetime , 
@CounselorEndTime datetime , 
@CanCounselorAcceptRequest bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSetting]( [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TSchoolKey,@ShouldVerifyCheckins,@CounselorStartTime,@CounselorEndTime,@CanCounselorAcceptRequest,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TSchoolKey uniqueidentifier  
		@ShouldVerifyCheckins bit  
		@CounselorStartTime datetime  
		@CounselorEndTime datetime  
		@CanCounselorAcceptRequest bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TSetting' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_Update]
@Key uniqueidentifier , 
@TSchoolKey uniqueidentifier , 
@ShouldVerifyCheckins bit , 
@CounselorStartTime datetime , 
@CounselorEndTime datetime , 
@CanCounselorAcceptRequest bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TSetting]
SET
	[TSchoolKey] = @TSchoolKey ,
	[ShouldVerifyCheckins] = @ShouldVerifyCheckins ,
	[CounselorStartTime] = @CounselorStartTime ,
	[CounselorEndTime] = @CounselorEndTime ,
	[CanCounselorAcceptRequest] = @CanCounselorAcceptRequest ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TSetting' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TSetting]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TSetting'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TSetting] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSetting]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TSetting]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TSetting'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TSetting_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TSetting]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSetting] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSetting] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TSetting' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TSetting] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TSetting] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TSetting]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSetting]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_SelectAllByForeignKeyTSchoolKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_SelectAllByForeignKeyTSchoolKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TSchoolKey],[ShouldVerifyCheckins],[CounselorStartTime],[CounselorEndTime],[CanCounselorAcceptRequest],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TSetting]
			WHERE [TSchoolKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSetting_DeleteAllByForeignKeyTSchoolKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSetting_DeleteAllByForeignKeyTSchoolKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSetting_DeleteAllByForeignKeyTSchoolKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSetting' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSetting_DeleteAllByForeignKeyTSchoolKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TSetting]
WHERE
	[TSchoolKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TStudent.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TStudent_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TStudent] DROP CONSTRAINT [TStudent_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TStudent'))
  BEGIN
      ALTER TABLE [dbo].[TStudent] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@StudentIdentifier varchar (50)  
		@IsMale bit  
		@CellPhone varchar (10)  
		@Email varchar (100)  
		@CanUseEmail bit  
		@CanUseCellPhone bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudent' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@StudentIdentifier varchar (50) , 
@IsMale bit , 
@CellPhone varchar (10) , 
@Email varchar (100) , 
@CanUseEmail bit , 
@CanUseCellPhone bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TStudent'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @FirstName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='FirstName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @FirstName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Username is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Username')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Username =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Password is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Password')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Password =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @StudentIdentifier is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='StudentIdentifier')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @StudentIdentifier =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsMale is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsMale')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsMale =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CellPhone is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CellPhone')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CellPhone =  convert ( varchar (10),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Email is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Email')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Email =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanUseEmail is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanUseEmail')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanUseEmail =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanUseCellPhone is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanUseCellPhone')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanUseCellPhone =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TStudent]( [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@FirstName,@LastName,@Username,@Password,@StudentIdentifier,@IsMale,@CellPhone,@Email,@CanUseEmail,@CanUseCellPhone,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TStudent_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@FirstName varchar (50) = null  
		@LastName varchar (50) = null  
		@Username varchar (50) = null  
		@Password varchar (50) = null  
		@StudentIdentifier varchar (50) = null  
		@IsMale bit = null  
		@CellPhone varchar (10) = null  
		@Email varchar (100) = null  
		@CanUseEmail bit = null  
		@CanUseCellPhone bit = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudent' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TStudent_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@FirstName varchar (50) = null , 
@LastName varchar (50) = null , 
@Username varchar (50) = null , 
@Password varchar (50) = null , 
@StudentIdentifier varchar (50) = null , 
@IsMale bit = null , 
@CellPhone varchar (10) = null , 
@Email varchar (100) = null , 
@CanUseEmail bit = null , 
@CanUseCellPhone bit = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TStudent'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @FirstName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='FirstName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @FirstName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Username is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Username')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Username =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Password is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Password')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Password =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @StudentIdentifier is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='StudentIdentifier')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @StudentIdentifier =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @IsMale is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='IsMale')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @IsMale =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CellPhone is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CellPhone')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CellPhone =  convert ( varchar (10),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Email is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Email')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Email =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanUseEmail is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanUseEmail')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanUseEmail =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CanUseCellPhone is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CanUseCellPhone')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CanUseCellPhone =  convert ( bit,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [FirstName], [LastName], [Username], [Password], [StudentIdentifier], [IsMale], [CellPhone], [Email], [CanUseEmail], [CanUseCellPhone], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TStudent] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TStudent]( [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[FirstName], INSERTED.[LastName], INSERTED.[Username], INSERTED.[Password], INSERTED.[StudentIdentifier], INSERTED.[IsMale], INSERTED.[CellPhone], INSERTED.[Email], INSERTED.[CanUseEmail], INSERTED.[CanUseCellPhone], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [FirstName], [LastName], [Username], [Password], [StudentIdentifier], [IsMale], [CellPhone], [Email], [CanUseEmail], [CanUseCellPhone], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@FirstName,@LastName,@Username,@Password,@StudentIdentifier,@IsMale,@CellPhone,@Email,@CanUseEmail,@CanUseCellPhone,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [FirstName], [LastName], [Username], [Password], [StudentIdentifier], [IsMale], [CellPhone], [Email], [CanUseEmail], [CanUseCellPhone], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@StudentIdentifier varchar (50)  
		@IsMale bit  
		@CellPhone varchar (10)  
		@Email varchar (100)  
		@CanUseEmail bit  
		@CanUseCellPhone bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudent' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_Insert]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@StudentIdentifier varchar (50) , 
@IsMale bit , 
@CellPhone varchar (10) , 
@Email varchar (100) , 
@CanUseEmail bit , 
@CanUseCellPhone bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TStudent]( [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@FirstName,@LastName,@Username,@Password,@StudentIdentifier,@IsMale,@CellPhone,@Email,@CanUseEmail,@CanUseCellPhone,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@StudentIdentifier varchar (50)  
		@IsMale bit  
		@CellPhone varchar (10)  
		@Email varchar (100)  
		@CanUseEmail bit  
		@CanUseCellPhone bit  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TStudent' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_Update]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@StudentIdentifier varchar (50) , 
@IsMale bit , 
@CellPhone varchar (10) , 
@Email varchar (100) , 
@CanUseEmail bit , 
@CanUseCellPhone bit , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TStudent]
SET
	[FirstName] = @FirstName ,
	[LastName] = @LastName ,
	[Username] = @Username ,
	[Password] = @Password ,
	[StudentIdentifier] = @StudentIdentifier ,
	[IsMale] = @IsMale ,
	[CellPhone] = @CellPhone ,
	[Email] = @Email ,
	[CanUseEmail] = @CanUseEmail ,
	[CanUseCellPhone] = @CanUseCellPhone ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TStudent' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TStudent]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TStudent'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TStudent] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudent' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TStudent]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TStudent' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TStudent]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TStudent'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TStudent_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TStudent]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TStudent' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TStudent] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TStudent] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TStudent' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TStudent] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[StudentIdentifier],[IsMale],[CellPhone],[Email],[CanUseEmail],[CanUseCellPhone],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TStudent] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TStudent' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TStudent]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectOneWithTAppointmentUsingTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectOneWithTAppointmentUsingTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectOneWithTAppointmentUsingTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudent' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectOneWithTAppointmentUsingTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TStudent_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyTStudentKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectOneWithTRequestUsingTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectOneWithTRequestUsingTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectOneWithTRequestUsingTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudent' and also the respective child records from 'TRequest'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectOneWithTRequestUsingTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TStudent_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TRequest_SelectAllByForeignKeyTStudentKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudent_SelectOneWithTStudentToCounselorUsingTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudent_SelectOneWithTStudentToCounselorUsingTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudent_SelectOneWithTStudentToCounselorUsingTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudent' and also the respective child records from 'TStudentToCounselor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudent_SelectOneWithTStudentToCounselorUsingTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TStudent_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				





-- Developer's comment header
-- TStudentToCounselor.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TStudentToCounselor_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TStudentToCounselor] DROP CONSTRAINT [TStudentToCounselor_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TStudentToCounselor'))
  BEGIN
      ALTER TABLE [dbo].[TStudentToCounselor] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudentToCounselor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TStudentToCounselor'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CounselorTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TStudentToCounselor]( [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TStudentKey,@CounselorTUserKey,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TStudentToCounselor_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@TStudentKey uniqueidentifier = null  
		@CounselorTUserKey uniqueidentifier = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudentToCounselor' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@TStudentKey uniqueidentifier = null , 
@CounselorTUserKey uniqueidentifier = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TStudentToCounselor'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStudentKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStudentKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @TStudentKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStudentKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @CounselorTUserKey is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CounselorTUserKey')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @CounselorTUserKey = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CounselorTUserKey =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [TStudentKey], [CounselorTUserKey], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TStudentToCounselor] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TStudentToCounselor]( [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[TStudentKey], INSERTED.[CounselorTUserKey], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [TStudentKey], [CounselorTUserKey], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@TStudentKey,@CounselorTUserKey,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [TStudentKey], [CounselorTUserKey], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TStudentToCounselor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_Insert]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TStudentToCounselor]( [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@TStudentKey,@CounselorTUserKey,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@TStudentKey uniqueidentifier  
		@CounselorTUserKey uniqueidentifier  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TStudentToCounselor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_Update]
@Key uniqueidentifier , 
@TStudentKey uniqueidentifier , 
@CounselorTUserKey uniqueidentifier , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TStudentToCounselor]
SET
	[TStudentKey] = @TStudentKey ,
	[CounselorTUserKey] = @CounselorTUserKey ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TStudentToCounselor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TStudentToCounselor]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TStudentToCounselor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TStudentToCounselor] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TStudentToCounselor]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TStudentToCounselor]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TStudentToCounselor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TStudentToCounselor_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TStudentToCounselor]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TStudentToCounselor] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TStudentToCounselor] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TStudentToCounselor' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TStudentToCounselor] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TStudentToCounselor] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TStudentToCounselor]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TStudentToCounselor]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TStudentToCounselor]
WHERE
	[CounselorTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyTStudentKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TStudentToCounselor]
			WHERE [TStudentKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKeyPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKeyPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKeyPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	:
		@Key uniqueidentifier 
		
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKeyPaged]
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table

if(@PageSize IS NOT NULL AND @PageSize <> '')
	SELECT TOP (@PageSize) *
	FROM (SELECT	[Key],[TStudentKey],[CounselorTUserKey],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY @OrderByStatement) AS SortRow 
			FROM	[dbo].[TStudentToCounselor]
			WHERE [CounselorTUserKey] = @Key) AS query 
	WHERE query.SortRow > @SkipPages * @PageSize

 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyTStudentKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyTStudentKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_DeleteAllByForeignKeyTStudentKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyTStudentKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TStudentToCounselor]
WHERE
	[TStudentKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TStudentToCounselor_DeleteAllByForeignKeyCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TStudentToCounselor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TStudentToCounselor_DeleteAllByForeignKeyCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
delete	
FROM	[dbo].[TStudentToCounselor]
WHERE
	[CounselorTUserKey] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				




-- Developer's comment header
-- TSupportedColor.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TSupportedColor_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TSupportedColor] DROP CONSTRAINT [TSupportedColor_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TSupportedColor'))
  BEGIN
      ALTER TABLE [dbo].[TSupportedColor] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (20)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSupportedColor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@Name varchar (20) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TSupportedColor'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (20),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSupportedColor]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TSupportedColor_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@Name varchar (20) = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSupportedColor' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@Name varchar (20) = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TSupportedColor'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @Name is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Name')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Name =  convert ( varchar (20),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TSupportedColor] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TSupportedColor]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[Name], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [Name], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (20)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TSupportedColor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_Insert]
@Key uniqueidentifier , 
@Name varchar (20) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TSupportedColor]( [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@Name,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@Name varchar (20)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TSupportedColor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_Update]
@Key uniqueidentifier , 
@Name varchar (20) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TSupportedColor]
SET
	[Name] = @Name ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TSupportedColor' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TSupportedColor]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TSupportedColor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TSupportedColor] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSupportedColor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TSupportedColor]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TSupportedColor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TSupportedColor]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TSupportedColor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TSupportedColor_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TSupportedColor]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TSupportedColor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSupportedColor] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TSupportedColor] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TSupportedColor' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TSupportedColor] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[Name],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TSupportedColor] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TSupportedColor' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TSupportedColor]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TSupportedColor_SelectOneWithTAppointmentTypeUsingTSupportedColorKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TSupportedColor_SelectOneWithTAppointmentTypeUsingTSupportedColorKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TSupportedColor_SelectOneWithTAppointmentTypeUsingTSupportedColorKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TSupportedColor' and also the respective child records from 'TAppointmentType'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TSupportedColor_SelectOneWithTAppointmentTypeUsingTSupportedColorKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TSupportedColor_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointmentType_SelectAllByForeignKeyTSupportedColorKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				





-- Developer's comment header
-- TUser.sql
-- 
-- history:   11/9/2015 3:46:06 PM
--
--


------------------------------------------------------------------------
  --DROP the IgnyteTimeStamp column that was needed for pessimistic locking
  IF EXISTS (SELECT 1 from sys.objects where name = 'TUser_IgnyteTimeStamp_DF')
	  ALTER TABLE [dbo].[TUser] DROP CONSTRAINT [TUser_IgnyteTimeStamp_DF]
  GO
  
  IF EXISTS(SELECT * FROM sys.columns WHERE Name = N'IgnyteTimeStamp' AND OBJECT_ID = OBJECT_ID(N'TUser'))
  BEGIN
      ALTER TABLE [dbo].[TUser] DROP COLUMN IgnyteTimeStamp
  END
  GO
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_Insert_WithDefaultValues]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_Insert_WithDefaultValues]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@Email varchar (100)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_Insert_WithDefaultValues]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@Email varchar (100) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS (nolock) where table_Name ='TUser'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   
          declare @___s nvarchar(4000)

          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @FirstName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='FirstName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @FirstName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Username is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Username')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Username =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Password is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Password')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Password =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Email is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Email')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Email =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          
          -- INSERT a new row in the table
          INSERT INTO [dbo].[TUser]( [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@FirstName,@LastName,@Username,@Password,@Email,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_Insert_WithDefaultValues_AndReturn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_Insert_WithDefaultValues_AndReturn]
GO


/*

OBJECT NAME : gsp_TUser_Insert_WithDefaultValues_AndReturn
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier = null  
		@FirstName varchar (50) = null  
		@LastName varchar (50) = null  
		@Username varchar (50) = null  
		@Password varchar (50) = null  
		@Email varchar (100) = null  
		@TStamp datetime = null  
		@DateCreated datetime = null  
		@CreatedBy varchar (50) = null  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50) = null  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TUser' and return back the entire row
(with the default values inserted)

*/
CREATE PROCEDURE [dbo].[gsp_TUser_Insert_WithDefaultValues_AndReturn]
@Key uniqueidentifier = null , 
@FirstName varchar (50) = null , 
@LastName varchar (50) = null , 
@Username varchar (50) = null , 
@Password varchar (50) = null , 
@Email varchar (100) = null , 
@TStamp datetime = null , 
@DateCreated datetime = null , 
@CreatedBy varchar (50) = null , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) = null , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

         
          -- create Table variable for columns default
          declare @____TEMP____TABLE___ table
          (
 	          column_name nvarchar(128),
 	          column_def nvarchar(4000)
          )
          insert into @____TEMP____TABLE___ SELECT column_name,column_default
          FROM INFORMATION_SCHEMA.COLUMNS   (nolock) where table_Name ='TUser'
      
          --prepare temporary variable to iterate over default values.
          declare @___t nvarchar(4000)   


          --variables for substring calculation to remove starting and ending parentheses is exist
          declare @__i bigint
          declare @__j bigint
          declare @__len bigint
          
          declare @____temp_execution_result___ table /*temp table*/
          (result nvarchar (4000))

          ---declare this once in every insertion stored procedure
          declare @___GUIDTable Table
          (
          [Key] [uniqueidentifier] NOT NULL DEFAULT (newsequentialid())
          )
          
          --test if the passes parameter is null to get its default
          if @Key is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Key')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          --the newsequentialid() can't be called outside table initializattion DDL
		      if lower (@___t) ='newsequentialid()' 
		      begin
              insert into @___GUIDTable ([key]) values(default)
              --for unique Identifier column use those lines
              set @Key = (select top (1) [key] from @___GUIDTable )
              delete @___GUIDTable --for other columns that miight come later 
			    end 
          else 
          begin     
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Key =  convert ( uniqueidentifier,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
          end
          
               

          end        
          --test if the passes parameter is null to get its default
          if @FirstName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='FirstName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @FirstName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastName is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastName')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastName =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Username is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Username')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Username =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Password is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Password')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Password =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Email is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Email')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Email =  convert ( varchar (100),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @TStamp is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='TStamp')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @TStamp =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @DateCreated is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='DateCreated')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @DateCreated =  convert ( datetime,(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @CreatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='CreatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @CreatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @LastUpdatedBy is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='LastUpdatedBy')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @LastUpdatedBy =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
          --test if the passes parameter is null to get its default
          if @Source is null
          begin
            set @___t=(select column_def from @____TEMP____TABLE___ where column_name='Source')
            --trim default value from leading and ending parentheses.
            set @__i=charindex ('(',@___t);
            set @__len = len (@___t)
            set @__j = charindex (')',@___t,@__len);

            if @__i= 1 AND @__j =@__len
            begin
              set @___t=substring( @___t,2,@__len-2);
            end
            
          
            set @___t = 'select '+@___t      
            -- execute the default value expression
            insert into @____temp_execution_result___ execute sp_executesql @___t
            set @Source =  convert ( varchar (50),(select result from @____temp_execution_result___));
            
            --clears temp table data to be ready for next insertion (for other procedure params)
            delete  from @____temp_execution_result___ 
               

          end        
            


          /* INSERT a new row in the table*/
          
            /*SELECT TOP 0 [Key], [FirstName], [LastName], [Username], [Password], [Email], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] INTO #tempTable FROM [dbo].[TUser] WHERE 1 = 2    */ 
            INSERT INTO [dbo].[TUser]( [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
            OUTPUT inserted.* /* INSERTED.[Key], INSERTED.[FirstName], INSERTED.[LastName], INSERTED.[Username], INSERTED.[Password], INSERTED.[Email], INSERTED.[TStamp], INSERTED.[DateCreated], INSERTED.[CreatedBy], INSERTED.[LastUpdatedBy], INSERTED.[Source]  into #tempTable( [Key], [FirstName], [LastName], [Username], [Password], [Email], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] )*/
            VALUES (   @Key,@FirstName,@LastName,@Username,@Password,@Email,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source  ) ; 

           /* SELECT [Key], [FirstName], [LastName], [Username], [Password], [Email], [TStamp], [DateCreated], [CreatedBy], [LastUpdatedBy], [Source] FROM #tempTable
            DROP TABLE  #tempTable */


/* Get the Error Code for the statment just executed*/
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_Insert]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_Insert
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@Email varchar (100)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will insert 1 row in the table 'TUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_Insert]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@Email varchar (100) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON


          -- INSERT a new row in the table
          INSERT INTO [dbo].[TUser]( [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source] )
          VALUES ( @Key,@FirstName,@LastName,@Username,@Password,@Email,@TStamp,@DateCreated,@CreatedBy,@LastUpdatedBy,@Source )


          -- Get the Error Code for the statment just executed
          SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_Update]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_Update
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  
		@FirstName varchar (50)  
		@LastName varchar (50)  
		@Username varchar (50)  
		@Password varchar (50)  
		@Email varchar (100)  
		@TStamp datetime  
		@DateCreated datetime  
		@CreatedBy varchar (50)  
		@LastUpdatedBy varchar (50) = null  
		@Source varchar (50)  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will update 1 row in the table 'TUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_Update]
@Key uniqueidentifier , 
@FirstName varchar (50) , 
@LastName varchar (50) , 
@Username varchar (50) , 
@Password varchar (50) , 
@Email varchar (100) , 
@TStamp datetime , 
@DateCreated datetime , 
@CreatedBy varchar (50) , 
@LastUpdatedBy varchar (50) = null , 
@Source varchar (50) , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- UPDATE a row in the table
UPDATE [dbo].[TUser]
SET
	[FirstName] = @FirstName ,
	[LastName] = @LastName ,
	[Username] = @Username ,
	[Password] = @Password ,
	[Email] = @Email ,
	[TStamp] = @TStamp ,
	[DateCreated] = @DateCreated ,
	[CreatedBy] = @CreatedBy ,
	[LastUpdatedBy] = @LastUpdatedBy ,
	[Source] = @Source
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_Delete]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_Delete
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier  

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete 1 row from the table 'TUser' 

----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_Delete]
@Key uniqueidentifier , 
@dlgErrorCode int OUTPUT

AS

SET NOCOUNT ON

-- DELETE a row from the table
DELETE FROM [dbo].[TUser]
WHERE
[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_DeleteByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_DeleteByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_DeleteByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will delete row(s) from the table 'TUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_DeleteByField]
@Field varchar(100),
@Value varchar(1000),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
-- DELETE row(s) from the table
DECLARE @query varchar(2000)

SET @query = 'DELETE FROM [dbo].[TUser] WHERE [' + @Field + '] = ''' + @Value + ''''
EXEC(@query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectByPrimaryKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectByPrimaryKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectByPrimaryKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectByPrimaryKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the table
SELECT	[Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
FROM	[dbo].[TUser]
WHERE
	[Key] = @Key


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectAll]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectAll
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows from the table 'TUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectAll]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
declare @Query nvarchar(max);
set @Query='SELECT	[Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM	[dbo].[TUser]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectAllPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectAllPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectAllPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table 'TUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectAllPaged]
@PageSize int=null,
@SkipPages int=null,
@OrderByStatement varchar(100)=null,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) from the table
DECLARE @Query nvarchar(max);

if(@PageSize is null or @PageSize='' or @SkipPages is null or @SkipPages='' or @OrderByStatement is null or @OrderByStatement='')
begin
Exec gsp_TUser_SelectAll @dlgErrorCode=@dlgErrorCode
end
else
begin
SET @Query='SELECT	[Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow 
			FROM	[dbo].[TUser]'

if(@PageSize is not null and @PageSize<>'')
	SET @Query='SELECT TOP ' + CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)
end
 EXEC (@Query)
  
 SELECT @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectByField]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectByField]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectByField
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select row(s) from the table 'TUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectByField]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query varchar(2000)

if @Value2 is not null and @Value2 <> ''
SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TUser] WHERE [' +replace(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' And '''+ @Value2+''''
else
SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source]
			FROM [dbo].[TUser] WHERE [' +replace(@Field,']',']]') + ']'+@Operation+ '''' + @Value + ''''

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectByFieldPaged]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectByFieldPaged]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectByFieldPaged
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Field varchar(100)
		@Value varchar(1000)
		@Value2 varchar(1000)
		@Operation varchar(10)
		@PageSize int
		@SkipPages int
		@OrderByStatement varchar(100)

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select the specified number of entries from the specified record number in the table'TUser' 
				using the value of the field specified
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectByFieldPaged]
@Field varchar(100),
@Value varchar(1000),
@Value2 varchar(1000)='',
@Operation varchar(10),
@PageSize int,
@SkipPages int,
@OrderByStatement varchar(100),
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON
SET @Value = REPLACE(@Value,'''','''''')
SET @Value2 = REPLACE(@Value2,'''','''''')
-- SELECT row(s) from the table
DECLARE @Query nvarchar(max);

IF @Value2 IS NOT NULL AND @Value2 <> ''
	SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement+']) AS SortRow
			FROM [dbo].[TUser] WHERE [' + REPLACE(@Field,']',']]') + '] BETWEEN  ''' + @Value + ''' AND '''+ @Value2+''''
ELSE
	SET @Query = 'SELECT [Key],[FirstName],[LastName],[Username],[Password],[Email],[TStamp],[DateCreated],[CreatedBy],[LastUpdatedBy],[Source], ROW_NUMBER() OVER(ORDER BY ['+ @OrderByStatement + ']) AS SortRow
			FROM [dbo].[TUser] WHERE [' + REPLACE(@Field,']',']]') + ']' + @Operation + '''' + @Value + ''''

IF(@PageSize IS NOT NULL AND @PageSize<>'')
	set @Query='SELECT TOP '+ CONVERT(varchar(max), @PageSize) + ' * FROM (' + @Query + ') AS query 
	WHERE query.SortRow > ' + CONVERT(varchar(max), @SkipPages * @PageSize)

EXEC(@Query)


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectAllCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectAllCount]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectAllCount
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select all rows count from the table 'TUser' 
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectAllCount]
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT all row(s) count from the table
declare @Query nvarchar(max);
set @Query='SELECT	Count(*)
			FROM	[dbo].[TUser]'

 exec (@Query)
  
 select @dlgErrorCode = @@ERROR


-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectOneWithTAppointmentUsingCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyCounselorTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCanceledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCanceledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectOneWithTAppointmentUsingCanceledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingCanceledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyCanceledByTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectOneWithTAppointmentUsingScheduledByTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingScheduledByTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectOneWithTAppointmentUsingScheduledByTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' and also the respective child records from 'TAppointment'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectOneWithTAppointmentUsingScheduledByTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TAppointment_SelectAllByForeignKeyScheduledByTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectOneWithTSchoolToTUserUsingTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectOneWithTSchoolToTUserUsingTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectOneWithTSchoolToTUserUsingTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' and also the respective child records from 'TSchoolToTUser'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectOneWithTSchoolToTUserUsingTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TSchoolToTUser_SelectAllByForeignKeyTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[gsp_TUser_SelectOneWithTStudentToCounselorUsingCounselorTUserKey]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[gsp_TUser_SelectOneWithTStudentToCounselorUsingCounselorTUserKey]
GO


/*
---------------------------------------------------------------------------------------------------
OBJECT NAME : gsp_TUser_SelectOneWithTStudentToCounselorUsingCounselorTUserKey
						
AUTHOR	:	Ignyte Software ©
DATE	:	11/9/2015 3:46:06 PM

INPUTS	: 
		@Key uniqueidentifier 

OUTPUTS	: 
		@Error     - The return code indicating if there was an error

DESCRIPTION : This stored procedure will select a row from the table 'TUser' and also the respective child records from 'TStudentToCounselor'
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[gsp_TUser_SelectOneWithTStudentToCounselorUsingCounselorTUserKey]
@Key uniqueidentifier ,
@dlgErrorCode int OUTPUT
AS

SET NOCOUNT ON

-- SELECT a row from the main table
EXEC gsp_TUser_SelectByPrimaryKey @Key = @Key ,@dlgErrorCode=@dlgErrorCode
EXEC gsp_TStudentToCounselor_SelectAllByForeignKeyCounselorTUserKey  @Key = @Key, @dlgErrorCode=@dlgErrorCode

-- Get the Error Code for the statment just executed
SET @dlgErrorCode = @@ERROR


GO
				






//
// Class	:	DatabaseHelper.cs
// Author	:  	Ignyte Software ©  2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:06 PM
//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Collections;
using System.Reflection;


namespace Ignyte.SeeMyCounselor.DataLayer
{

    #region Enumerations

    public enum SupportedDatabases
    {
        SqlServer = 0,
        OleDb,
        Oracle,
        ODBC,
        ConfigurationDefined,
        SqlCe
    }

    public enum ConnectionState
    {
        KeepOpen, CloseOnExit
    }

    #endregion

    public class DatabaseHelper : IDisposable
    {
        [System.Runtime.InteropServices.DllImport("Rpcrt4.dll")]
        private static extern int UuidCreateSequential(ref Guid guid);

        #region Class Level Variables

        private string _connectionString        = string.Empty;
        public DbConnection _newConnection      = null;
        public DbDataReader DataReader          = null;
        private DbTransaction _transaction      = null;
        private DbCommand _newCommand           = null;
        private DbProviderFactory _newFactory   = null;
        private bool _shouldLogErrors           = true;
        private string _errorLogFileName        = "Generic DataAccess Error Log File.txt";
        private bool _shouldLogSQLToCompactFile = false;
        private bool _shouldLogSQLToDetailFile  = false;
        private bool _isDisposed                = false;
        private int _commandTimeout             = 0;

        #endregion

        #region Constants

        private const string DEFAULT_CONNECTION_STRING_KEY_NAME = "Production"; // default to using the production defined connection string
        private const string PROVIDER_SQL_SERVER_CLIENT = "System.Data.SqlClient";
        private const string PROVIDER_OLEDB_CLIENT = "System.Data.OleDb";
        private const string PROVIDER_ODBC_CLIENT = "System.Data.Odbc";
        private const string PROVIDER_SQL_CE_CLIENT = "System.Data.SqlServerCe";

        #endregion

        #region Constructors / Destructors

        /// <summary>
        /// This constructor allows an instance of the data access library to be created and cached
        /// assuming a valid connection string is provided along with the enumeration indicating 
        /// which database provider to utilize.  In the case of a standard application configuration 
        /// file not being utilized, this would be the constructor to use.
        /// </summary>
        ///
        /// <param name="connectionString" type = "string">The fully compliant .NET connection string</param>
        /// <param name="databaseToUse" type="SupportedDatabases">Indicates the type of database to connect with</param>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author			      	  	Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public DatabaseHelper(SupportedDatabases databaseToUse, string connectionString)
        {
            CacheConnection(GetFactory(GetFactoryProviderName(databaseToUse)), connectionString);
        }


        /// <summary>
        /// This constructor allows an instance of the data access library to be created and cached with a connection string and database provider.
        /// It determines the type of database to instantiate by looking at the configuration section of the config file.
        /// </summary>
        ///
        /// <param name="configFileAndPath" type = "string">The path and file name containing the connection string defintion to be used.</param>
        /// <param name="configFileConnectionStringKeyName" type = "string">The key name in the config file that defines the connection string to be used.</param>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author			        		Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public DatabaseHelper(string configFileAndPath)
        {
            try
            {
                CacheConnection(GetFactory(ConnectionStringHelper.GetInfo(configFileAndPath).ProviderName), ConnectionStringHelper.GetInfo(configFileAndPath).ConnectionString);
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }

        }

        /// <summary>
        /// This constructor allows an instance of the data access library to be created and cached with a connection string and database provider.
        /// It determines the type of database to instantiate by looking at the configuration section of the config file.
        /// </summary>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public DatabaseHelper()
        {
            try
            {
                CacheConnection(GetFactory(ConnectionStringHelper.GetInfo().ProviderName), ConnectionStringHelper.GetInfo().ConnectionString);
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }

        }


        public void Dispose()
        {
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Executes in two distinct scenarios:
        // If isDisposed equals true, the method has been called directly
        // or indirectly by user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool isDisposing)
        {
            // Check to see if Dispose has already been called.
            if (!_isDisposed)
            {
                // If isDisposing equals true cleanup all managed and unmanaged resources.
                if (isDisposing)
                {
                    // Dispose managed resources.
                    _newConnection.Close();
                    _newConnection.Dispose();
                    _newCommand.Dispose();
                    _newCommand = null;
                    _newConnection = null;
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here.
                // If isDisposing is false, 
                // only the following code is executed.

            }
            _isDisposed = true;
        }

        #endregion

        #region Properties

		public int CommandTimeOut
		{
			get
			{
				return _commandTimeout;
			}
			set
			{
				_commandTimeout = value;
			}
		}
    
    public bool ShouldLogSQLToDetailFile
        {
            get
            {

                // We need to check to see if a configuration key exists in the 
                // config file as this will override the setting of the property
                // by the programmer.

                return (ConfigurationManager.AppSettings["ShouldLogSQLToDetailFile"] != null ? ConfigurationManager.AppSettings["ShouldLogSQLToDetailFile"].ToUpper().Equals("TRUE") : _shouldLogSQLToDetailFile); ;
            }
            set
            {
                _shouldLogSQLToDetailFile = value;
            }
        }
        public string connectionString
        {
            get 
            {
                return _connectionString;
            }
        }
        
        
        public string SQLLogCompactFileName
        {
            get
            {
                return SQLLoggingHelper.SQL_Compact_Log_FileName;
            }
            set
            {
                SQLLoggingHelper.SQL_Compact_Log_FileName = value;
            }
        }

        public bool ShouldLogSQLToCompactFile
        {
            get
            {
                // We need to check to see if a configuration key exists in the 
                // config file as this will override the setting of the property
                // by the programmer.

                return (ConfigurationManager.AppSettings["ShouldLogSQLToCompactFile"] != null ? ConfigurationManager.AppSettings["ShouldLogSQLToCompactFile"].ToUpper().Equals("TRUE") : _shouldLogSQLToCompactFile); ;

            }
            set
            {
                _shouldLogSQLToCompactFile = value;
            }
        }

        public string SQLLogDetailFileName
        {
            get
            {
                return SQLLoggingHelper.SQL_Detail_Log_FileName;
            }
            set
            {
                SQLLoggingHelper.SQL_Detail_Log_FileName = value;
            }
        }

        public bool ShouldLogErrors
        {
            get
            {
                return _shouldLogErrors;
            }
            set
            {
                _shouldLogErrors = value;
            }
        }

        public string ErrorLogFileName
        {
            get
            {
                return _errorLogFileName;
            }
            set
            {
                _errorLogFileName = value;
            }
        }

        public DbCommand Command
        {
            get
            {
                return _newCommand;
            }
            set
            {
                _newCommand = value;
            }
        }

        public DbTransaction CurrentTransaction
        {
            get
            {
                return _transaction;
            }
            set
            {
                _transaction = value;
            }
        }
        
        #endregion

        #region Methods (Private)

        /// <summary>
        /// Creates a connection and caches it so that it can be used later to submit SQL.
        /// </summary>
        ///
        /// <param name="factory" type = "DbProviderFactory">The factory to create the connection against.</param>
        /// <param name="connectionString" type = "string">The connection string used to make the connnection to the database.</param>
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private void CacheConnection(DbProviderFactory factory, string connectionString)
        {
            try
            {

                // create a connection using the built-in factory and cache
                // the command object.
                _connectionString = connectionString; // set class level instance and cache connection string
                _newFactory = factory;
                _newConnection = factory.CreateConnection();
                _newCommand = factory.CreateCommand();

                _newConnection.ConnectionString = connectionString;
                _newCommand.Connection = _newConnection;
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }
        }

        /// <summary>
        /// This method will return a factory instance of a specific database provider based on 
        /// the connection string defined in the ConnectionStrings section of the configuration file.
        /// </summary>
        ///
        /// <param name="connectStringsKeyName" type="string">The name of the connection string to retrieve from the ConnectionStrings section of the configuration file.</param>
        ///
        /// <returns>An instance of the specific factory object requested.</returns>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private DbProviderFactory GetFactory(string providerName)
        {
            DbProviderFactory newFactory = null;

            try
            {
                //string providername = ConfigurationManager.ConnectionStrings[connectStringsKeyName].ProviderName;
                switch (providerName)
                {
                    case PROVIDER_SQL_SERVER_CLIENT:
                        newFactory = SqlClientFactory.Instance;
                        break;
                    case PROVIDER_OLEDB_CLIENT:
                        newFactory = OleDbFactory.Instance;
                        break;
                    case PROVIDER_ODBC_CLIENT:
                        newFactory = OdbcFactory.Instance;
                        break;
					
                }
            }
            catch (Exception ex)
            {
                ProcessException(ex);
            }

            return newFactory;

        }

        /// <summary>
        /// Creates an instance of the factory provider defined in the default connection string section.
        /// </summary>
        ///
        /// <returns>A database provider factory</returns>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private DbProviderFactory GetFactory()
        {
            return GetFactory(DEFAULT_CONNECTION_STRING_KEY_NAME);
        }


        /// <summary>
        /// This method will determine the name of the identified enumerated database.
        /// </summary>
        ///
        /// <param name="databaseToUse" type="SupportedDatabases">The enumeration of the database in which to find it's name.</param>
        ///
        /// <returns>The name of the database factory provider.</returns>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private string GetFactoryProviderName(SupportedDatabases databaseToUse)
        {
            string newFactoryName = string.Empty;

            switch (databaseToUse)
            {
                case SupportedDatabases.SqlServer:
                    newFactoryName = "System.Data.SqlClient";
                    break;

                case SupportedDatabases.OleDb:
                    newFactoryName = "System.Data.OleDb";
                    break;

                case SupportedDatabases.Oracle:
                    newFactoryName = "System.Data.OracleClient";
                    break;

                case SupportedDatabases.ODBC:
                    newFactoryName = "System.Data.Odbc";
                    break;
                case SupportedDatabases.SqlCe:
                    newFactoryName = "System.Data.SqlServerCe";
                    break;
            }

            return newFactoryName;
        }


        /// <summary>
        /// This method determines if an exception should be recorded, records it (if needed) and rethrows the exception.
        /// </summary>
        ///
        /// <param name="databaseToUse" type="SupportedDatabases">The enumeration of the database in which to find it's name.</param>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private void ProcessException(Exception ex)
        {
            if (ShouldLogErrors)
            {
                if (ex != null)
                    WriteToLog(ex.Message);
                else
                    WriteToLog("Exception could not be logged because it was NULL");
            }

            throw ex;   // rethrow error
        }

        /// <summary>
        /// This method writes the given text to a text file.
        /// </summary>
        ///
        /// <param name="text" type="string">The information to write to the file.</param>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        private void WriteToLog(string text)
        {
            try
            {
                StreamWriter writer = File.AppendText(ErrorLogFileName);
                writer.WriteLine(DateTime.Now.ToString("yyyy/MM/dd hh:mm") + " - " + text);
                writer.Close();
            }
            catch
            {
                // if we can't log the error then eat the exception and continue.
            }

        }

        private void LogSQLStatementsIfNecessary(ref DbCommand commandToLog)
        {
            // deterimine if the application has been configured to record
            // sql statements to a file.

            if (ShouldLogSQLToCompactFile && ShouldLogSQLToDetailFile)
            {
                SQLLoggingHelper.LogSQLStatement(ref commandToLog, SQLLoggingHelper.LoggingStyle.Both);
            }
            else if (ShouldLogSQLToCompactFile)
            {
                SQLLoggingHelper.LogSQLStatement(ref commandToLog, SQLLoggingHelper.LoggingStyle.Compact);
            }
            else if (ShouldLogSQLToDetailFile)
            {
                SQLLoggingHelper.LogSQLStatement(ref commandToLog, SQLLoggingHelper.LoggingStyle.Detailed);
            }
        }

        //private void DeriveParameters()
        //{
        //    _newFactory.de
        //}

        #endregion

        #region Methods (Public)

        /// <summary>
        /// This method generates a sequential GUID.
        /// </summary>
        ///
        /// <returns>A sequential GUID.</returns>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static string GetSequentialGUID()
        {
            Guid newGuid = new Guid();
            int returnValue = UuidCreateSequential(ref newGuid);
            return newGuid.ToString();
        }


        // ------------------------------------
        // Explicit Transaction Related Methods
        // ------------------------------------

        /// <summary>
        /// Marks the beginning of a transaction with a default Isolation Level of ReadUncommited.
        /// </summary>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public void BeginTransaction()
        {
            BeginTransaction(IsolationLevel.ReadCommitted);
        }

        /// <summary>
        /// Marks the beginning of a transaction.
        /// </summary>
        ///
        /// <param name="isolationLevel" type="IsolationLevel">The locking level in which to run this transaction.</param>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            if (_newConnection.State == System.Data.ConnectionState.Closed)
                _newConnection.Open();

            CurrentTransaction = _newConnection.BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Commits a transaction.
        /// </summary>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public void CommitTransaction()
        {
            CurrentTransaction.Commit();
        }

        /// <summary>
        /// Rollbacks a transaction.
        /// </summary>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public void RollbackTransaction()
        {
            CurrentTransaction.Rollback();
        }


        // -------------------------
        // Parameter Related Methods
        // -------------------------

        /// <summary>
        /// Creates a new parameter on the current factory.
        /// </summary>
        ///	
        /// <returns>A DbParameter object representing a parameter to pass to a stored procedure.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public DbParameter CreateParameter()
        {
            return _newFactory.CreateParameter();
        }

        /// <summary>
        /// Adds a parameter to the call list prior to executing a stored procedure.
        /// </summary>
        ///
        /// <param name="parameter" type="DbParameter">The parameter to pass to the stored procedure.</param>
        ///	
        /// <returns>An integer representing the number of parameters added to the collection.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public int AddParameter(DbParameter parameter)
        {
            return _newCommand.Parameters.Add(parameter);
        }

        /// <summary>
        /// Adds a parameter to the call list prior to executing a stored procedure.
        /// </summary>
        ///
        /// <param name="name" type="string">The name of the parameter as defined in the stored procedure.</param>
        /// <param name="value" type="object">The value of the parameter to pass to the stored procedure.</param> 
        /// 
        ///	
        /// <returns>An integer representing the number of parameters currently in the collection.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public int AddParameter(string name, object value)
        {
            int result = 0;
            DbParameter p = _newFactory.CreateParameter();

            p.ParameterName = name;
            p.Value = value;

            result = _newCommand.Parameters.Add(p);
            return result;
        }

        /// <summary>
        /// Adds a parameter to the current command parameters collection.
        /// </summary>
        ///	
        /// <param name="name" type="">The name of the parameter to add to the collection.</param>
        /// <param name="value" type="">The value of the parameter.</param>
        /// <param name="dataType" type="">The data type the parameter.</param>
        /// <param name="direction" type="">The direction the variable.</param>
        /// 
        /// <returns>The number of parameters in the collection.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		    Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public int AddParameter(string name, object value, DbType dataType, ParameterDirection direction)
        {
            int result = 0;
            DbParameter p = _newFactory.CreateParameter();

            p.ParameterName = name;
            p.Value = value;
            p.Direction = direction;
            p.DbType = dataType;

            result = _newCommand.Parameters.Add(p);

            return result;
        }

        /// <summary>
        /// Adds a parameter to the current command parameters collection.
        /// </summary>
        ///	
        /// <param name="name" type="">The name of the parameter to add to the collection.</param>
        /// <param name="value" type="">The value of the parameter.</param>
        /// <param name="dataType" type="">The data type the parameter.</param>
        /// 
        /// <returns>The number of parameters in the collection.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		    Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public int AddParameter(string name, object value, DbType dataType)
        {
            int result = 0;
            DbParameter p = _newFactory.CreateParameter();

            p.ParameterName = name;
            p.Value = value;
            p.DbType = dataType;

            result = _newCommand.Parameters.Add(p);

            return result;
        }

        /// <summary>
        /// Adds a parameter to the current command parameters collection.
        /// </summary>
        ///	
        /// <param name="name" type="">The name of the parameter to add to the collection.</param>
        /// <param name="value" type="">The value of the parameter.</param>
        /// <param name="direction" type="">The direction the variable.</param>
        /// 
        /// <returns>The number of parameters in the collection.</returns>
        /// 
        /// <remarks>
        ///	
        /// <RevisionHistory>
        /// Author			        		Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public int AddParameter(string name, object value, ParameterDirection direction)
        {
            int result = 0;
            DbParameter p = _newFactory.CreateParameter();

            p.ParameterName = name;
            p.Value = value;
            p.Direction = direction;

            result = _newCommand.Parameters.Add(p);

            return result;
        }


        // ------------------------
        // NonQuery Related Methods
        // ------------------------

        public int ExecuteNonQuery(string spName)
        {
            bool executionSucceeded = false;
            return ExecuteNonQuery(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public int ExecuteNonQuery(string spName, ref bool executionSucceeded)
        {
            return ExecuteNonQuery(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public int ExecuteNonQuery(string sqlStatement, CommandType commandType)
        {
            bool executionSucceeded = false;
            return ExecuteNonQuery(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public int ExecuteNonQuery(string sqlStatement, CommandType commandType, ref bool executionSucceeded)
        {
            return ExecuteNonQuery(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public int ExecuteNonQuery(string sqlStatement, CommandType commandType, ConnectionState connectionState, ref bool executionSucceeded)
        {
            _newCommand.CommandText = sqlStatement;
            _newCommand.CommandType = commandType;
            _newCommand.CommandTimeout = _commandTimeout;
            
            int i = 0;
            try
            {
                if (!TransactionHelper.IsInTransactionMode && _newConnection.State == System.Data.ConnectionState.Closed)
                {
                    _newConnection.Open();
                }
                if (TransactionHelper.IsInTransactionMode)
                    i = TransactionHelper.ExecuteNonQuery((DatabaseHelper)this.MemberwiseClone());
                else
                    i = _newCommand.ExecuteNonQuery();
                executionSucceeded = true;
            }
            catch (Exception ex)
            {
                executionSucceeded = false;
                ProcessException(ex);
            }
            finally
            {
                // Revision: MWO - 4-18-2013
                // Added sql statement logging 
                LogSQLStatementsIfNecessary(ref _newCommand);

                _newCommand.Parameters.Clear();
                if (connectionState == ConnectionState.CloseOnExit && _newCommand.Transaction == null)
                {
                    _newConnection.Close();
                }
            }

            return i;
        }



        // ----------------------
        // Scalar Related Methods
        // ----------------------

        public object ExecuteScalar(string spName)
        {
            bool executionSucceeded = false;
            return ExecuteScalar(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public object ExecuteScalar(string spName, ref bool executionSucceeded)
        {
            return ExecuteScalar(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public object ExecuteScalar(string sqlStatement, CommandType commandType)
        {
            bool executionSucceeded = false;
            return ExecuteScalar(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public object ExecuteScalar(string sqlStatement, CommandType commandType, ref bool executionSucceeded)
        {
            return ExecuteScalar(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public object ExecuteScalar(string sqlStatement, CommandType commandType, ConnectionState connectionState, ref bool executionSucceeded)
        {
            _newCommand.CommandText = sqlStatement;
            _newCommand.CommandType = commandType;
            _newCommand.CommandTimeout = _commandTimeout;
            
            object result = null;
            try
            {
                if (!TransactionHelper.IsInTransactionMode && _newConnection.State == System.Data.ConnectionState.Closed)
                {
                    _newConnection.Open();
                }
                if (TransactionHelper.IsInTransactionMode)
                    result = TransactionHelper.ExecuteScalar((DatabaseHelper)this.MemberwiseClone());
                else
                    result = _newCommand.ExecuteScalar();
                executionSucceeded = true;
            }
            catch (Exception ex)
            {
                executionSucceeded = false;
                ProcessException(ex);
            }
            finally
            {
                // Revision: MWO - 4-18-2013
                // Added sql statement logging 
                LogSQLStatementsIfNecessary(ref _newCommand);

                _newCommand.Parameters.Clear();
                if (connectionState == ConnectionState.CloseOnExit)
                {
                    _newConnection.Close();
                }
            }

            return result;
        }


        // -----------------------------
        // ExecuteReader Related Methods
        // -----------------------------

        public DbDataReader ExecuteReaderUsingSQL(string sqlStatement)
        {
            bool executionSucceeded = false;
            return ExecuteReader(sqlStatement, CommandType.Text, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReaderUsingSQL(string sqlStatement, ref bool executionSucceeded)
        {
            return ExecuteReader(sqlStatement, CommandType.Text, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReader(string spName)
        {
            bool executionSucceeded = false;
            return ExecuteReader(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReader(string spName, ref bool executionSucceeded)
        {
            return ExecuteReader(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReader(string sqlStatement, CommandType commandType)
        {
            bool executionSucceeded = false;
            return ExecuteReader(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReader(string sqlStatement, CommandType commandType, ref bool executionSucceeded)
        {
            return ExecuteReader(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DbDataReader ExecuteReader(string sqlStatement, CommandType commandType, ConnectionState connectionState, ref bool executionSucceeded)
        {
            _newCommand.CommandText = sqlStatement;
            _newCommand.CommandType = commandType;
            _newCommand.CommandTimeout = _commandTimeout;

            try
            {
                if (!TransactionHelper.IsInTransactionMode && _newConnection.State == System.Data.ConnectionState.Closed)
                {
                    _newConnection.Open();
                }
                if (connectionState == ConnectionState.CloseOnExit)
                {
                    if (TransactionHelper.IsInTransactionMode)
                        DataReader = TransactionHelper.ExecuteReader((DatabaseHelper)this.MemberwiseClone());
                    else
                        DataReader = _newCommand.ExecuteReader(CommandBehavior.CloseConnection);
                }
                else
                {
                    if (TransactionHelper.IsInTransactionMode)
                        DataReader = TransactionHelper.ExecuteReader((DatabaseHelper)this.MemberwiseClone());
                    else
                        DataReader = _newCommand.ExecuteReader();
                }
                executionSucceeded = true;
            }
            catch (Exception ex)
            {
                executionSucceeded = false;
                ProcessException(ex);
            }
            finally
            {
                // Revision: MWO - 4-18-2013
                // Added sql statement logging 
                LogSQLStatementsIfNecessary(ref _newCommand);

                _newCommand.Parameters.Clear();

            }

            return DataReader;
        }

        public DataSet ExecuteDataSet(string spName)
        {
            bool executionSucceeded = false;
            return ExecuteDataSet(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DataSet ExecuteDataSet(string spName, ref bool executionSucceeded)
        {
            return ExecuteDataSet(spName, CommandType.StoredProcedure, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DataSet ExecuteDataSet(string sqlStatement, CommandType commandType)
        {
            bool executionSucceeded = false;
            return ExecuteDataSet(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DataSet ExecuteDataSet(string sqlStatement, CommandType commandType, ref bool executionSucceeded)
        {
            return ExecuteDataSet(sqlStatement, commandType, ConnectionState.CloseOnExit, ref executionSucceeded);
        }

        public DataSet ExecuteDataSet(string sqlStatement, CommandType commandType, ConnectionState connectionState, ref bool executionSucceeded)
        {
            DataSet result = new DataSet();
            DbDataAdapter adapter = _newFactory.CreateDataAdapter();

            _newCommand.CommandText = sqlStatement;
            _newCommand.CommandType = commandType;
            _newCommand.CommandTimeout = _commandTimeout;
            adapter.SelectCommand = _newCommand;

            try
            {
                if (TransactionHelper.IsInTransactionMode)
                    result = TransactionHelper.ExecuteDataSet(this, adapter);
                else
                    adapter.Fill(result);
                executionSucceeded = true;
            }
            catch (Exception ex)
            {
                executionSucceeded = false;
                ProcessException(ex);
            }
            finally
            {

                // Revision: MWO - 4-18-2013
                // Added sql statement logging 
                LogSQLStatementsIfNecessary(ref _newCommand);

                _newCommand.Parameters.Clear();
                if (connectionState == ConnectionState.CloseOnExit)
                {
                    if (_newConnection.State == System.Data.ConnectionState.Open)
                    {
                        _newConnection.Close();
                    }
                }
            }
            return result;
        }
        
        
        /// <summary>
        /// This method copies the content of one object to another regardless of type.
        /// </summary>
        ///
        /// <returns>A number of properties affected.</returns>
        /// 
        /// <param name="from" type = "object">The reference to the source object.</param>
        /// <param name="to" type = "object">The reference to the result pobject.</param>
        /// <param name="ignoreProperties" type = "string">A simple comma delimited string of properties to ignore when copying.</param>
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		    11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>        
        public static int CopyProperties(object from, object to, string ignoreProperties)
        {
            int nCount = 0;
            Dictionary<string, object> dictionary = DictionaryFromType(from);
            ignoreProperties += ",";
            Type t = to.GetType();
            PropertyInfo[] props = t.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                try
                {
                    if (ignoreProperties.ToLower().IndexOf(prop.Name.ToLower() + ",") < 0)
                    {
                        if (dictionary.ContainsKey(prop.Name))
                            prop.SetValue(to, dictionary[prop.Name], null);
                        nCount++;
                    }
                }
                catch (Exception)
                {
                }
            }
            return nCount;
        }

        private static Dictionary<string, object> DictionaryFromType(object atype)
        {
            if (atype == null)
                return new Dictionary<string, object>();
            Type t = atype.GetType();
            PropertyInfo[] props = t.GetProperties();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (PropertyInfo prp in props)
            {
                object value = prp.GetValue(atype, new object[] { });
                dict.Add(prp.Name, value);
            }
            return dict;
        }
        
        #endregion

    }

    #region Internal Classes

    //internal static class ObjectExtensions
    //{

    //    public static bool IsInt32(this object o)
    //    {
    //        if (o == null) return false;

    //        if (ObjectExtensions.IsString(o))
    //        {
    //            int r;
    //            return int.TryParse(o.ToString(), out r);
    //        }
    //        else
    //        {
    //            return o is Int32;
    //        }
    //    }

    //    public static bool IsFloat(this object o)
    //    {
    //        if (o == null) return false;

    //        if (ObjectExtensions.IsString(o))
    //        {
    //            float r;
    //            return float.TryParse(o.ToString(), out r);
    //        }
    //        else
    //        {
    //            return o is float;
    //        }
    //    }

    //    public static bool IsString(this object o)
    //    {
    //        if (o == null) return false;
    //        return o is String;
    //    }

    //    public static bool IsBoolean(this object o)
    //    {
    //        if (o == null) return false;

    //        if (ObjectExtensions.IsString(o))
    //        {
    //            bool r;
    //            return bool.TryParse(o.ToString(), out r);
    //        }
    //        else
    //        {
    //            return o is Boolean;
    //        }
    //    }


    //    public static int ToInt32OrZero(this object o)
    //    {
    //        return ObjectExtensions.ToInt32OrDefault(o, 0);
    //    }

    //    public static int ToInt32OrDefault(this object o, int def)
    //    {
    //        if (o == null) return def;

    //        if (o.IsInt32()) 
    //            return Convert.ToInt32(o);
    //        else
    //            return def;

    //    }

    //    public static float ToFloatOrZero(this object o)
    //    {
    //        return ObjectExtensions.ToFloatOrDefault(o, 0.0f);
    //    }

    //    public static float ToFloatOrDefault(this object o, float def)
    //    {
    //        if (o == null) return def;

    //        if (o.IsFloat())
    //            return (float)Convert.ToDouble(o);
    //        else
    //            return def;
    //    }



    //    public static bool ToBooleanOrDefault(this object o, bool def)
    //    {
    //        if (o == null) return def;

    //        if (o.IsBoolean())
    //            return Convert.ToBoolean(o);
    //        else
    //            return def;
    //    }

    //}

    //internal static class StringExtensions
    //{

    //    //public static string StripHtml(this String s)
    //    //{

    //    //    return RegexHelper.StripHtml(s);

    //    //}

    //    public static int ToInt32OrZero(this String s)
    //    {
    //        return ((object)s).ToInt32OrZero();
    //    }

    //    public static int ToInt32OrDefault(this String s, int def)
    //    {
    //        return ((object)s).ToInt32OrDefault(def);
    //    }

    //    public static float ToFloatOrZero(this String s)
    //    {
    //        return ((object)s).ToFloatOrZero();
    //    }

    //    public static float ToFloatOrDefault(this String s, float def)
    //    {
    //        return ((object)s).ToFloatOrDefault(def);
    //    }

    //    public static bool ToBooleanOrDefault(this String s, bool def)
    //    {
    //        return ((object)s).ToBooleanOrDefault(def);
    //    }

    //}

    /// <summary>
    /// This class provides the functionality to log sql statements and stored procedure
    /// calls to a text for debugging and logging purposes.
    /// </summary>
    internal static class SQLLoggingHelper
    {

        public enum LoggingStyle
        {
            Compact = 0,
            Detailed = 1,
            Both = 2
        }

        public static string SQL_Compact_Log_FileName = "SQL Compact Log File.txt";
        public static string SQL_Detail_Log_FileName = "SQL Detail Log File.txt";


        /// <summary>
        /// This method will take a Database parameter and convert it to a proper
        /// value for a SQL statement.
        /// </summary>
        ///
        /// <param name="dp" type = "DbParameter">The generic database parameter to convert.</param>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author			      	  	Date		            Description
        /// DLGenerator		    11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static string ParameterValueFormattedForSQLStatement(DbParameter dp)
        {
            string returnValue = string.Empty;

            switch (dp.DbType)
            {
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.String:
                case DbType.StringFixedLength:
                case DbType.Guid:
                case DbType.Time:
                case DbType.Xml:
                case DbType.Date:
                case DbType.DateTime:
                case DbType.DateTime2:
                case DbType.DateTimeOffset:
                    returnValue = "'" + dp.Value.ToString().Replace("'", "''") + "'";
                    break;

                // bit can null, 0, or 1
                case DbType.Boolean:
                    string answer = string.Empty;

                    if (dp.Value == null)
                        answer = "0";
                    else
                    {
                        int bitValue = 0;
                        int.TryParse(dp.Value.ToString(), out bitValue);
                        answer = bitValue.ToString();
                    }

                    returnValue = answer;

                    //returnValue = (sp.Value.ToBooleanOrDefault(false)) ? "1" : "0";
                    break;

                default:
                    returnValue = dp.Value.ToString().Replace("'", "''");
                    break;
            }

            return returnValue;
        }

        /// <summary>
        /// This method will take a DbCommand object format and log it based upon
        /// it's command text, parameter names, and parameter values.
        /// </summary>
        ///
        /// <param name="cmd" type = "DbCommand">The DbCommand to format and log.</param>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author			      	  	Date		            Description
        /// DLGenerator		    11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static void LogSQLStatement(ref DbCommand cmd, LoggingStyle style)
        {


            if (cmd.CommandText != null && cmd.CommandText.Length > 0)
            {

                if (style == LoggingStyle.Detailed)
                {
                    LogDetailedSQLStatement(ref cmd, SQL_Detail_Log_FileName);
                }
                else if (style == LoggingStyle.Compact)
                {
                    LogCompactSQLStatement(ref cmd, SQL_Compact_Log_FileName);
                }
                else
                {
                    LogDetailedSQLStatement(ref cmd, SQL_Detail_Log_FileName);
                    LogCompactSQLStatement(ref cmd, SQL_Compact_Log_FileName);
                }
            }
        }

        private static void LogCompactSQLStatement(ref DbCommand cmd, string compactFileName)
        {
            string textToRecord = cmd.CommandText;

            foreach (DbParameter p in cmd.Parameters)
            {
                textToRecord = textToRecord.Replace(p.ParameterName, ParameterValueFormattedForSQLStatement(p));
            }

            // Record to a local file.
            File.AppendAllText(compactFileName, textToRecord + System.Environment.NewLine);
        }

        private static void LogDetailedSQLStatement(ref DbCommand cmd, string detailedFileName)
        {

            StringBuilder msg = new StringBuilder();
            string textToRecord = string.Empty;
            int maxLengthOfaLineOfText = 0;

            textToRecord = string.Format("\r\nRecorded @:\t{0}", DateTime.Now.ToString("MMMM dd, yyyy hh:mm:ss tt"));
            maxLengthOfaLineOfText = (textToRecord.Length > maxLengthOfaLineOfText) ? textToRecord.Length : maxLengthOfaLineOfText;
            msg.AppendLine(textToRecord);

            textToRecord = string.Format("Database:\t{0}", cmd.Connection.Database);
            maxLengthOfaLineOfText = (textToRecord.Length > maxLengthOfaLineOfText) ? textToRecord.Length : maxLengthOfaLineOfText;
            msg.AppendLine(textToRecord);

            textToRecord = string.Format("Statement:\t{0}", cmd.CommandText);
            maxLengthOfaLineOfText = (textToRecord.Length > maxLengthOfaLineOfText) ? textToRecord.Length : maxLengthOfaLineOfText;
            msg.AppendLine(textToRecord);
            msg.AppendLine("Paramters:");

            foreach (DbParameter p in cmd.Parameters)
            {
                textToRecord = string.Format("\t\t{0}: {1}", p.ParameterName, p.Value.ToString());
                msg.AppendLine(textToRecord);
                maxLengthOfaLineOfText = (textToRecord.Length > maxLengthOfaLineOfText) ? textToRecord.Length : maxLengthOfaLineOfText;
            }

            // We have built the contents of the actual message so lets letter box it
            msg.Insert(0, new string('*', maxLengthOfaLineOfText + 10));
            msg.AppendLine(new string('*', maxLengthOfaLineOfText + 10));

            // Record to a local file.
            File.AppendAllText(detailedFileName, msg.ToString() + System.Environment.NewLine);

            msg.Clear();
        }

        public static void LogSQLStatement(ref DbCommand cmd)
        {
            LogSQLStatement(ref cmd, LoggingStyle.Detailed);
        }

        public static string DatabaseCommandAsTSQL(DbCommand cmd)
        {
            StringBuilder sql = new StringBuilder();
            bool isFirstParam = true;

            sql.AppendLine("use " + cmd.Connection.Database + ";");
            switch (cmd.CommandType)
            {
                case CommandType.StoredProcedure:
                    sql.AppendLine("declare @return_value int;");

                    foreach (DbParameter dp in cmd.Parameters)
                    {
                        if ((dp.Direction == ParameterDirection.InputOutput) || (dp.Direction == ParameterDirection.Output))
                        {
                            sql.Append("declare " + dp.ParameterName + "\t" + dp.DbType.ToString() + "\t= ");

                            sql.AppendLine(((dp.Direction == ParameterDirection.Output) ? "null" : ParameterValueFormattedForSQLStatement(dp)) + ";");

                        }
                    }

                    sql.AppendLine("exec [" + cmd.CommandText + "]");

                    foreach (DbParameter dp in cmd.Parameters)
                    {
                        if (dp.Direction != ParameterDirection.ReturnValue)
                        {
                            sql.Append((isFirstParam) ? "\t" : "\t, ");

                            if (isFirstParam) isFirstParam = false;

                            if (dp.Direction == ParameterDirection.Input)
                                sql.AppendLine(dp.ParameterName + " = " + ParameterValueFormattedForSQLStatement(dp));
                            else

                                sql.AppendLine(dp.ParameterName + " = " + dp.ParameterName + " output");
                        }
                    }
                    sql.AppendLine(";");

                    sql.AppendLine("select 'Return Value' = convert(varchar, @return_value);");

                    foreach (SqlParameter sp in cmd.Parameters)
                    {
                        if ((sp.Direction == ParameterDirection.InputOutput) || (sp.Direction == ParameterDirection.Output))
                        {
                            sql.AppendLine("select '" + sp.ParameterName + "' = convert(varchar, " + sp.ParameterName + ");");
                        }
                    }
                    break;
                case CommandType.Text:
                    sql.AppendLine(cmd.CommandText);
                    break;
            }

            return sql.ToString();
        }

    }

    /// <summary>
    /// This class allows us to define our connection string once and reuse it
    /// across calls to the database, basically a caching mechanism.
    /// </summary>
    internal static class ConnectionStringHelper
    {
        private static ConnectionInfo _connectionInfo = new ConnectionInfo();

        internal struct ConnectionInfo
        {
            public string ConnectionString;
            public string ProviderName;
        }

        public static ConnectionInfo GetInfo()
        {
            return GetInfo(string.Empty);
        }

        public static bool IsConnectionStringLoaded()
        {
            return (_connectionInfo.ConnectionString != null && _connectionInfo.ConnectionString.Length > 0);
        }

        public static ConnectionInfo GetInfo(string configFileAndPath)
        {

            if (_connectionInfo.ConnectionString != null)
            {
                // if the connection information has already been retrieved
                // then return it so we don't have to read the config file 
                // again.
                return _connectionInfo;
            }
            else
            {

                // Our static variable is empty so we need to find our config
                // file (or use what was passed in) and load the appropriate 
                // connection string and provider.  For web apps, the config file
                // could exist as the 'web.config' but for windows apps, it would 
                // app.config where 'app' is replaced with the name of the actual
                // application executable name.
                try
                {
                    if (configFileAndPath.Trim().Length == 0)
                    {
                        // If the file does not exist then try and find one...
                        if (!System.IO.File.Exists(configFileAndPath))
                        {
                            // First, we will look for the default ignyte data layer generator config file
                            // in the bin directory of the application's base directory.  If it is
                            // not there, we will look in the root directory.
                            configFileAndPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "bin", "Ignyte.SeeMyCounselor.DataLayer.config");
                            if (File.Exists(configFileAndPath) == false)
                                configFileAndPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Ignyte.SeeMyCounselor.DataLayer.config");

                            // If the file still does not exists, then we might be dealing with a web application
                            // which will store it's settings in the web.config file.
                            if (File.Exists(configFileAndPath) == false)
                                configFileAndPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "web.config");

                            // If the file still cannot be found, then it could be using it's own app.config file.  
                            // The 'app' portion of the name will need to be changed to match the name of the application.
                            if (File.Exists(configFileAndPath) == false)
                                configFileAndPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, System.Reflection.Assembly.GetEntryAssembly().GetName().Name + ".config");
                        }
                    }

                    // Make sure the file exists.
                    if (System.IO.File.Exists(configFileAndPath))
                    {
                        ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
                        Configuration configFile = null;
                        string keyToUse = string.Empty;

                        fileMap.ExeConfigFilename = configFileAndPath;
                        configFile = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

                        keyToUse = configFile.AppSettings.Settings["ConnectionStringToUse"].Value;

                        _connectionInfo.ConnectionString = configFile.ConnectionStrings.ConnectionStrings[keyToUse].ConnectionString;
                        _connectionInfo.ProviderName = configFile.ConnectionStrings.ConnectionStrings[keyToUse].ProviderName;
                        
                        
                    }
                    else
                    {
                        throw new ConfigurationErrorsException();
                    }

                    //

                }
                catch (ConfigurationErrorsException ex)
                {
                    // determine if the data access config file even exists as
                    // this might be the problem.

                    if (!System.IO.File.Exists(configFileAndPath))
                    {
                        throw new System.IO.FileNotFoundException("The data access configuration file (" + configFileAndPath + ") was not found.  Please copy this file to the same location as the Generic Data Access DLL.", ex);
                    }
                    else
                    {
                        throw;
                    }

                }
                catch (Exception)
                {
                    throw;
                }

                return _connectionInfo;
            }

        }
    }
    
    #region Transaction Helper Class
        
    public static class TransactionHelper
    {
      
        [ThreadStatic]
        private static IsolationLevel _isolationLevel;
        [ThreadStatic]
        public static DatabaseHelper ExecHelper;
        [ThreadStatic]
        public static bool IsInTransactionMode;

        public static IsolationLevel TransactionIsolationLevel
        {
            get
            {
                return _isolationLevel;
            }
            set
            {
                _isolationLevel = value;
            }
        }        

        /// <summary>
        /// Marks the beginning of a transaction.
        /// </summary>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static void BeginTransaction()
        {
            BeginTransaction(IsolationLevel.ReadCommitted);
        }

        /// <summary>
        /// Marks the beginning of a transaction.
        /// </summary>
        ///
        /// <param name="isolationLevel" type="IsolationLevel">The locking level in which to run this transaction.</param>
        /// 
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static void BeginTransaction(IsolationLevel isolationlevel)
        {
            TransactionIsolationLevel = isolationlevel;

            ExecHelper = new DatabaseHelper();
            if (ExecHelper._newConnection.State == System.Data.ConnectionState.Closed)
                ExecHelper._newConnection.Open();

            IsInTransactionMode = true;
        }

        /// <summary>
        /// Commits a transaction.
        /// </summary>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static void CommitTransaction()
        {
            if (ExecHelper.CurrentTransaction != null)
                ExecHelper.CurrentTransaction.Commit();

            if (ExecHelper.DataReader != null)
            {
                ExecHelper.DataReader.Close();
                ExecHelper.DataReader.Dispose();
            }

            ExecHelper._newConnection.Close();
            ExecHelper.CurrentTransaction = null;
            ExecHelper = null;
            IsInTransactionMode = false;
        }

        /// <summary>
        /// Rolls back and clears a transaction.
        /// </summary>
        ///
        ///	<remarks>
        ///	
        /// <RevisionHistory>
        /// Author					        Date		          Description
        /// DLGenerator		11/9/2015 3:46:06 PM			Created function
        /// 
        /// </RevisionHistory>
        /// 
        /// </remarks>
        public static void RollbackTransaction()
        {
            if (ExecHelper.CurrentTransaction != null)
            {
                if (ExecHelper.DataReader != null)
                {
                    ExecHelper.DataReader.Close();
                    ExecHelper.DataReader.Dispose();
                }
                
                ExecHelper.Command.Cancel();
                ExecHelper.Command.Dispose();

                ExecHelper.CurrentTransaction.Rollback();
            }

            ExecHelper._newConnection.Close();
            ExecHelper.CurrentTransaction = null;
            ExecHelper = null;
            IsInTransactionMode = false;
        }
        
        private static void PrepareCommand(DatabaseHelper dbh)
        {
            ExecHelper.Command = dbh.Command;
            ExecHelper.Command.Connection = ExecHelper._newConnection;

            if (ExecHelper.CurrentTransaction == null)
                ExecHelper.BeginTransaction(TransactionIsolationLevel);

            ExecHelper.Command.Transaction = ExecHelper.CurrentTransaction;
        }
        
        private static void PrepareAdapter(DatabaseHelper dbh, ref DbDataAdapter adapter, ref DataSet result)
        {
            ExecHelper.Command = dbh.Command;
            ExecHelper.Command.Connection = ExecHelper._newConnection;

            if (ExecHelper.CurrentTransaction == null)
                ExecHelper.BeginTransaction(TransactionIsolationLevel);

            ExecHelper.Command.Transaction = ExecHelper.CurrentTransaction;
        }

        public static object ExecuteScalar(DatabaseHelper dbh)
        {
            PrepareCommand(dbh);
            return ExecHelper.Command.ExecuteScalar();
        }

        public static DbDataReader ExecuteReader(DatabaseHelper dbh)
        {
            PrepareCommand(dbh);
            return ExecHelper.Command.ExecuteReader();
        }

        public static int ExecuteNonQuery(DatabaseHelper dbh)
        {
            PrepareCommand(dbh);
            return ExecHelper.Command.ExecuteNonQuery();
        }

        public static DataSet ExecuteDataSet(DatabaseHelper dbh, DbDataAdapter adapter)
        {
            DataSet result = new DataSet();
            adapter.SelectCommand.Connection = ExecHelper._newConnection;

            if (ExecHelper.CurrentTransaction == null)
                ExecHelper.BeginTransaction(TransactionIsolationLevel);
            
            adapter.SelectCommand.Transaction = ExecHelper.CurrentTransaction;

            adapter.Fill(result);
            return result;
        }        
    }
        
    #endregion
    

    #endregion
}

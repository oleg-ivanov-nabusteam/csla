//
// Class	:	TSchoolToTUser.cs
// Author	:  	Ignyte Software © 2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:08 PM
//

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Collections;
using System.Data.Common;

namespace Ignyte.SeeMyCounselor.DataLayer
{
	
	/// <summary>
	/// Data access class for the "TSchoolToTUser" table.
	/// </summary>
	[Serializable]
	public class TSchoolToTUser : TSchoolToTUserBase
	{
	
		#region Class Level Variables

		#endregion
		
		#region Constants
		
		#endregion

		#region Constructors / Destructors 
		
		public TSchoolToTUser() : base()
		{
		}

		#endregion

		#region Properties

		#endregion

		#region Methods (Public)

		#endregion
		
		#region Methods (Private)

		#endregion

	}
	
}

//
// Class	:	TSchoolToTUserPrimaryKey.cs
// Author	:  	Ignyte Software © 2011 (DLG 3.0.0.3)
// Date		:	11/9/2015 3:46:08 PM
//

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;

namespace Ignyte.SeeMyCounselor.DataLayer
{
	public class TSchoolToTUserPrimaryKey
	{

	#region Class Level Variables
			private Guid?          	_keyNonDefault           	= null;
	#endregion

	#region Constants

	#endregion

	#region Constructors / Destructors

		/// <summary>
		/// Constructor setting values for all fields
		/// </summary>
		public TSchoolToTUserPrimaryKey(Guid? key) 
		{
	
			
			this._keyNonDefault = key;

		}

		#endregion

	#region Properties

		/// <summary>
		/// This property is mapped to the "Key" field.  Mandatory.
		/// </summary>
		public Guid? Key
		{
			get 
			{ 
				return _keyNonDefault;
			}
			set 
			{
			
				_keyNonDefault = value; 
			}
		}		//This property is related to the table name that exist in database
		public static string tableName
		{
			get 
			{ 
				  return "TSchoolToTUser";
			}
		}

		#endregion

	#region Methods (Public)

		/// <summary>
		/// Method to get the list of fields and their values
		/// </summary>
		///
		/// <returns>Name value collection containing the fields and the values</returns>
		///
		/// <remarks>
		///
		/// <RevisionHistory>
		/// Author				Date			Description
		/// DLGenerator			11/9/2015 3:46:08 PM				Created function
		/// 
		/// </RevisionHistory>
		///
		/// </remarks>
		///
		public NameValueCollection GetKeysAndValues() 
		{
			NameValueCollection nvc=new NameValueCollection();
			
			nvc.Add("Key",_keyNonDefault.ToString());
			return nvc;
			
		}

		#endregion	
		
	#region Methods (Private)

	#endregion

	}
}

﻿using System;
using System.Collections.Generic;
using Ignyte.SeeMyCounselor.BusinessLayer;

namespace Ignyte.SeeMyCounselor.Website
{
    public class StudentAppointmentsPresenter
    {
        private readonly IStudentAppointmentsView _studentAppointmentsView;

        public StudentAppointmentsPresenter(IStudentAppointmentsView studentAppointmentsView)
        {
            _studentAppointmentsView = studentAppointmentsView;
        }

        public void DisplayStudentAppointments()
        {
            var studentAppointmentList = StudentAppointmentList.GetStudentAppointmentList(
                Guid.Parse("00000000-0000-0000-0000-000000000000"), // John Legend Student Key
                _studentAppointmentsView.AppointmentDate);

            var studentAppointmentViewModels = GetStudentAppointmentViewModelsFrom(studentAppointmentList);

            _studentAppointmentsView.DisplayStudentAppointments(studentAppointmentViewModels);
        }

        private static IEnumerable<StudentAppointmentViewModel> GetStudentAppointmentViewModelsFrom(
            StudentAppointmentList studentAppointmentList)
        {
            var studentAppointmentViewModels = new List<StudentAppointmentViewModel>();

            foreach (var studentAppointment in studentAppointmentList)
            {
                var studentAppointmentViewModel = new StudentAppointmentViewModel
                {
                    CounselorFullName = string.Format("{0} {1}", studentAppointment.CounselorFirstName,
                        studentAppointment.CounselorLastName),
                    ScheduledFrom = string.Format("{0:HH:mm}", studentAppointment.DateTimeScheduledFrom),
                    ScheduledTo = string.Format("{0:HH:mm}", studentAppointment.DateTimeScheduledTo),
                    AppointmentType = studentAppointment.AppointmentTypeDescription,
                    SupportedColorHtmlCode = ColorConverter.ConvertToHtmlCode(studentAppointment.SupportedColorName)
                };

                studentAppointmentViewModels.Add(studentAppointmentViewModel);
            }

            return studentAppointmentViewModels;
        }
    }
}

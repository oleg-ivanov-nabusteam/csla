﻿using System;
using System.Collections.Generic;

namespace Ignyte.SeeMyCounselor.Website
{
    public interface IStudentAppointmentsView
    {
        DateTime AppointmentDate { get; }
        void DisplayStudentAppointments(IEnumerable<StudentAppointmentViewModel> studentAppointments);
    }
}

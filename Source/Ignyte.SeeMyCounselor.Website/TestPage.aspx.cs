﻿using System;
using System.Collections.Generic;
using System.Web.UI;

namespace Ignyte.SeeMyCounselor.Website
{
    public partial class TestPage : Page, IStudentAppointmentsView
    {
        private StudentAppointmentsPresenter _studentAppointmentsPresenter;

        protected void Page_Init(object sender, EventArgs e)
        {
            _studentAppointmentsPresenter = new StudentAppointmentsPresenter(this);

            AppointmentDateCalendar.SelectionChanged += AppointmentDateCalendarOnSelectionChanged;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            AppointmentDateCalendar.SelectedDate = DateTime.UtcNow;
            AppointmentDateCalendar.AutoPostBack = true;
            AppointmentDateCalendar.ShowClearButton = false;

            StudentAppointmentsGridView.Settings.ShowColumnHeaders = false;

            _studentAppointmentsPresenter.DisplayStudentAppointments();
        }

        public DateTime AppointmentDate
        {
            get { return AppointmentDateCalendar.SelectedDate; }
        }

        public void DisplayStudentAppointments(IEnumerable<StudentAppointmentViewModel> studentAppointments)
        {
            StudentAppointmentsGridView.DataSource = studentAppointments;
            StudentAppointmentsGridView.DataBind();
        }

        private void AppointmentDateCalendarOnSelectionChanged(object sender, EventArgs eventArgs)
        {
            _studentAppointmentsPresenter.DisplayStudentAppointments();
        }
    }
}

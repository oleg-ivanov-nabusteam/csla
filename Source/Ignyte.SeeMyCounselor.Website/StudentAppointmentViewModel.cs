﻿namespace Ignyte.SeeMyCounselor.Website
{
    public class StudentAppointmentViewModel
    {
        public string CounselorFullName { get; set; }
        public string ScheduledFrom { get; set; }
        public string ScheduledTo { get; set; }
        public string AppointmentType { get; set; }
        public string SupportedColorHtmlCode { get; set; }
    }
}

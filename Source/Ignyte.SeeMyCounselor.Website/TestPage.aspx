﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="Ignyte.SeeMyCounselor.Website.TestPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <br />
    <span>Choose date to show appointments:</span>
    <br />
    <br />
    <dx:ASPxCalendar ID="AppointmentDateCalendar" runat="server" />
    <br />
    <div style="width: 100%">
        <dx:ASPxGridView ID="StudentAppointmentsGridView" runat="server" Width="50%">
            <Templates>
                <DataRow>
                    <div style="padding: 5px;">
                        <table style="width: 100%; border: black solid 1px;">
                            <tr>
                                <td style="width: 5%; background: <%# Eval("SupportedColorHtmlCode") %>;"></td>
                                <td style="width: 40%;">
                                    <h1><%# Eval("CounselorFullName") %></h1>
                                    <h4>- <%# Eval("AppointmentType") %> -</h4>
                                </td>
                                <td>
                                    <h2><%# Eval("ScheduledFrom") %> - <%# Eval("ScheduledTo") %></h2>
                                </td>
                                <td>
                                    <a href="javascript:;" style="color: red;">CANCEL THIS APPOINTMENT</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                </DataRow>
            </Templates>
        </dx:ASPxGridView>
    </div>
    <br />
    <br />
    <br />
</asp:Content>

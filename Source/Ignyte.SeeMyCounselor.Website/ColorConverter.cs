﻿using System;

namespace Ignyte.SeeMyCounselor.Website
{
    public static class ColorConverter
    {
        public static string ConvertToHtmlCode(string colorName)
        {
            switch (colorName)
            {
                case "Red":
                    return "#FF0000";
                case "Green":
                    return "#008000";
                case "Blue":
                    return "#0000FF";
                default:
                    throw new ArgumentOutOfRangeException(colorName);
            }
        }
    }
}

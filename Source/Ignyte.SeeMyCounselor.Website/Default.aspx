﻿<%@ Page Title="Title" Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Ignyte.SeeMyCounselor.Website.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
           <header>
                <!-- HEADER -->
                <div class="content">
                    <div class="header-content">
                        <div class="header-pic-right">
                            <!--<img alt="image" src="img/header-pic-right.png" height="326" width="314"> -->
                            <asp:Image ID="imgHeaderRight" runat="server" ImageUrl="~/Content/img/header-pic-right.png" Height="326" Width="314" />
                        </div>
                        <h1>THE COUNSELOR IS YOUR POINT PERSON FOR SETTING UP MEETINGS, ACCESSING RESOURCES, AND PROVIDING SUPPORT WITH ACADEMIC AND SOCIAL/EMOTIONAL ISSUES.</h1>
                        <button class="header-button">Make An Appointment With Counselor</button>
                    </div>
                </div>
            </header>
            <!-- END HEADER --> 

    <!-- CONTENT AREA -->
    <div class="content-area">

        <section class="s1">
            <!-- SECTION #1 -->
            <div class="content">
                <div class="section-1">
                    <!-- CONTENT -->
                    <h1>Welcome to <span style="color: #1c97c3">See My Counselor</span>
                    </h1>
                    <div>
                        <div class="photo">
                            <!-- <img alt="image" src="img/photo-1.jpg" height="100" width="100" class="section-1-photo"> -->
                            <asp:Image ID="imgSectionPhoto" runat="server" ImageUrl="~/Content/img/photo-1.jpg" Height="100" Width="100" />

                        </div>
                        <p class="section-1-text">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempo incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud extiation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorelipsi eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt ut labore et dolore magna aliqua ut enim ad minim
                        </p>
                    </div>
                </div>
                <!-- END CONTENT -->

                <div class="sign-in">
                    <!-- SIGN IN FORM-->
                    <h1>Sign In</h1>
                    <p>
                        <!-- <input type="text" class="sign-input"> -->
                        <asp:TextBox ID="txtUsername" runat="server" CssClass="sign-input">

                        </asp:TextBox>

                    </p>
                    <p>
                        <!-- <input type="text" class="sign-input"> -->
                        <asp:TextBox ID="txtPassword" runat="server" CssClass="sign-input">

                        </asp:TextBox>
                    </p>
                    <p>
                        <%--<input type="checkbox" class="sign-checkbox"> <span class="sign-checkbox-text">Would you like us to remember you?</span>--%>
                        <asp:CheckBox ID="chkRemember" runat="server" /><span class="sign-checkbox-text">Would you like us to remember you?</span>
                    </p>
                    <p>
                        <button class="sign-in-button">
                            <%--<img alt="image" src="img/login-icon.png" height="16" width="11" />--%>
                            <asp:Image ID="imgLogin" runat="server" ImageUrl="~/Content/img/login-icon.png" Height="16" Width="11" />
                            <span>Login</span>
                        </button>
                        <span class="pass-forgot">
                            <a href="#">Forgot your password ?</a>
                        </span>
                    </p>
                </div>
                <!-- END SIGN IN FORM-->
            </div>
        </section>
        <!-- END SECTION #1 -->
        <section class="s2">
            <!-- SECTION #2 -->
            <div class="content">
                <div class="section-2">
                    <!-- CONTENT -->

                    <div class="section-2-left">
                        <!-- LEFT CONTENT -->
                        <div class="section-2-title">
                            <!-- TITLE -->
                            <h1>Latest <span style="color: #1c97c3">News</span>
                            </h1>
                            <div class="arrows">
                                <!-- ARROWS -->
                                <!-- <img alt="image" src="img/left-arrow.png" height="25" width="25"> -->
                                <a href="">
                                    <asp:Image ID="imgLeftArrow" runat="server" ImageUrl="~/Content/img/left-arrow.png" Height="25" Width="25" AlternateText="image" />
                                </a>
                                 
                                <!-- <img alt="image" src="img/right-arrow.png" height="25" width="25"> -->
                                <a href="">
                                   <asp:Image ID="imgRightArrow" runat="server" ImageUrl="~/Content/img/right-arrow.png" Height="25" Width="25"  AlternateText="image" />
                                </a>
                            </div>
                            <!-- END ARROWS -->
                        </div>
                        <!-- END TITLE -->

                        <div class="row-post">
                            <!-- ROW -->
                            <div class="news-post">
                                <!-- POST -->
                                <div class="date">
                                    <span class="month">DEC</span>
                                    <span class="day">20</span>
                                </div>
                                <div class="post">
                                    <h3>Lorem Ipsum is simply dummy</h3>
                                    Lorem ipsum dolor sit amet, consec tetur adipisicing elit, sed 
                                </div>
                            </div>
                            <!-- END POST -->
                            <div class="news-post">
                                <!-- POST -->
                                <div class="date">
                                    <span class="month">DEC</span>
                                    <span class="day">20</span>
                                </div>
                                <div class="post">
                                    <h3>Lorem Ipsum is simply dummy</h3>
                                    Lorem ipsum dolor sit amet, consec tetur adipisicing elit, sed
                                </div>
                            </div>
                            <!-- END POST -->
                        </div>
                        <!-- ROW -->

                        <div class="row-post">
                            <!-- ROW -->
                            <div class="news-post">
                                <!-- POST -->
                                <div class="date">
                                    <span class="month">DEC</span>
                                    <span class="day">20</span>
                                </div>
                                <div class="post">
                                    <h3>Lorem Ipsum is simply dummy</h3>
                                    Lorem ipsum dolor sit amet, consec tetur adipisicing elit, sed 
                                </div>
                            </div>
                            <!-- END POST -->
                            <div class="news-post">
                                <!-- POST -->
                                <div class="date">
                                    <span class="month">DEC</span>
                                    <span class="day">20</span>
                                </div>
                                <div class="post">
                                    <h3>Lorem Ipsum is simply dummy</h3>
                                    Lorem ipsum dolor sit amet, consec tetur adipisicing elit, sed
                                </div>
                            </div>
                            <!-- END POST -->
                        </div>
                        <!-- ROW -->

                    </div>
                    <!-- END LEFT CONTENT -->

                    <div class="section-2-right">
                        <!-- RIGHT CONTENT -->
                        <div class="section-2-title">
                            <!-- TITLE -->
                            <h1>Advertisement <span style="color: #1c97c3">Here</span>
                            </h1>
                            <div class="arrows">
                                <!-- ARROWS -->
                                <!-- <img alt="image" src="img/left-arrow.png" height="25" width="25"> -->
                                <a href="">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Content/img/left-arrow.png" Height="25" Width="25" AlternateText="image" />
                                </a>
                                 
                                <!-- <img alt="image" src="img/right-arrow.png" height="25" width="25"> -->
                                <a href="">
                                   <asp:Image ID="Image2" runat="server" ImageUrl="~/Content/img/right-arrow.png" Height="25" Width="25"  AlternateText="image" />
                                </a>
                            </div>
                            <!-- END ARROWS -->
                        </div>
                        <!-- END TITLE -->

                        <div class="adv-row">
                            <!-- ADV ROW -->
                            <div class="adv-item">
                                <!-- <img alt="image" src="img/adv-1.png" height="125" width="125"> -->
                                <asp:Image ID="imgAdv1" runat="server" ImageUrl="~/Content/img/adv-1.png" Height="125" Width="125"  AlternateText="image" />
                            </div>
                            <div class="adv-item">
                              <!-- <img alt="image" src="img/adv-2.png" height="125" width="125"> -->
                                <asp:Image ID="imgAdv2" runat="server" ImageUrl="~/Content/img/adv-2.png" Height="125" Width="125"  AlternateText="image" />
                            </div>
                            <div class="adv-item">
                               <!-- <img alt="image" src="img/adv-3.png" height="125" width="125"> -->
                                <asp:Image ID="imgAdv3" runat="server" ImageUrl="~/Content/img/adv-3.png" Height="125" Width="125"  AlternateText="image" />
                            </div>
                        </div>
                        <!-- END ADV ROW -->

                    </div>
                    <!-- END RIGHT CONTENT -->

                </div>
                <!-- END CONTENT -->
            </div>
        </section>
        <!-- END SECTION #2 -->
    </div>
    <!-- END CONTENT AREA -->

</asp:Content>

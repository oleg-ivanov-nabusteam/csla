﻿/* Create SCHOOLS */

DECLARE @SchoolKey UNIQUEIDENTIFIER
SET @SchoolKey = NEWID()

INSERT INTO [dbo].[TSchool]
	([Key]
	,[Name]
	,[AddressLine1]
	,[AddressLine2]
	,[City]
	,[StateOrProvince]
	,[ZipOrPostalCode]
	,[IsActive]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@SchoolKey
	,'Boys High School'
	,'Corduff'
	,'Blanchardstown'
	,'Sacramento'
	,'CA'
	,'5423432'
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

/* Create SUPPORTED COLORS */

DECLARE @RedSupportedColorKey UNIQUEIDENTIFIER
SET @RedSupportedColorKey = NEWID()

DECLARE @GreenSupportedColorKey UNIQUEIDENTIFIER
SET @GreenSupportedColorKey = NEWID()

DECLARE @BlueSupportedColorKey UNIQUEIDENTIFIER
SET @BlueSupportedColorKey = NEWID()

INSERT INTO [dbo].[TSupportedColor]
	([Key]
	,[Name]
	,[TStamp]
	,[DateCreated]	
	,[CreatedBy]
	,[Source])
VALUES
	(@RedSupportedColorKey
	,'Red'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TSupportedColor]
	([Key]
	,[Name]
	,[TStamp]
	,[DateCreated]	
	,[CreatedBy]
	,[Source])
VALUES
	(@GreenSupportedColorKey
	,'Green'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TSupportedColor]
	([Key]
	,[Name]
	,[TStamp]
	,[DateCreated]	
	,[CreatedBy]
	,[Source])
VALUES
	(@BlueSupportedColorKey
	,'Blue'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

/* Create APPOINTMENT TYPES */

DECLARE @CollegeInformationAppointmentTypeKey UNIQUEIDENTIFIER
SET @CollegeInformationAppointmentTypeKey = NEWID()

DECLARE @GradesAppointmentTypeKey UNIQUEIDENTIFIER
SET @GradesAppointmentTypeKey = NEWID()

DECLARE @PersonalProblemAppointmentTypeKey UNIQUEIDENTIFIER
SET @PersonalProblemAppointmentTypeKey = NEWID()

INSERT INTO [dbo].[TAppointmentType]
	([Key]
	,[TSchoolKey]
	,[TSupportedColorKey]
	,[Description]
	,[LengthOfTimeInMinutes]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@CollegeInformationAppointmentTypeKey
	,@SchoolKey
	,@RedSupportedColorKey
	,'College information'
	,30
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TAppointmentType]
	([Key]
	,[TSchoolKey]
	,[TSupportedColorKey]
	,[Description]
	,[LengthOfTimeInMinutes]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@GradesAppointmentTypeKey
	,@SchoolKey
	,@GreenSupportedColorKey
	,'Grades'
	,25
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TAppointmentType]
	([Key]
	,[TSchoolKey]
	,[TSupportedColorKey]
	,[Description]
	,[LengthOfTimeInMinutes]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@PersonalProblemAppointmentTypeKey
	,@SchoolKey
	,@BlueSupportedColorKey
	,'Personal Problem'
	,60
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

/* Create STUDENTS */

DECLARE @JohnLegendStudentKey UNIQUEIDENTIFIER
SET @JohnLegendStudentKey = CAST('00000000-0000-0000-0000-000000000000' AS UNIQUEIDENTIFIER)

DECLARE @AnnaSmithStudentKey UNIQUEIDENTIFIER
SET @AnnaSmithStudentKey = NEWID()

INSERT INTO [dbo].[TStudent]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[StudentIdentifier]
	,[IsMale]
	,[CellPhone]
	,[Email]
	,[CanUseEmail]
	,[CanUseCellPhone]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@JohnLegendStudentKey
	,'John'
	,'Legend'
	,'Test'
	,'Test'
	,'Test'
	,1
	,'Test'
	,'Test'
	,1
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TStudent]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[StudentIdentifier]
	,[IsMale]
	,[CellPhone]
	,[Email]
	,[CanUseEmail]
	,[CanUseCellPhone]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@AnnaSmithStudentKey
	,'Anna'
	,'Smith'
	,'Test'
	,'Test'
	,'Test'
	,0
	,'Test'
	,'Test'
	,1
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

/* Create USERS */

DECLARE @JamesFrancoUserKey UNIQUEIDENTIFIER
SET @JamesFrancoUserKey = NEWID()

DECLARE @DeanSmithUserKey UNIQUEIDENTIFIER
SET @DeanSmithUserKey = NEWID()

DECLARE @JanetJonhesUserKey UNIQUEIDENTIFIER
SET @JanetJonhesUserKey = NEWID()

DECLARE @TestUserKey UNIQUEIDENTIFIER
SET @TestUserKey = NEWID()

INSERT INTO [dbo].[TUser]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[Email]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@JamesFrancoUserKey
	,'James'
	,'Franco'
	,'Test'
	,'Test'
	,'Test'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TUser]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[Email]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@DeanSmithUserKey
	,'Dean'
	,'Smith'
	,'Test'
	,'Test'
	,'Test'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TUser]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[Email]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@JanetJonhesUserKey
	,'Janet'
	,'Jonhes'
	,'Test'
	,'Test'
	,'Test'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TUser]
	([Key]
	,[FirstName]
	,[LastName]
	,[Username]
	,[Password]
	,[Email]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@TestUserKey
	,'Test'
	,'Test'
	,'Test'
	,'Test'
	,'Test'
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

/* Create APPOINTMENTS */

INSERT INTO [dbo].[TAppointment]
	([TSchoolKey]
	,[CounselorTUserKey]
	,[TStudentKey]
	,[ScheduledByTUserKey]
	,[TAppointmentTypeKey]
	,[CanceledByTUserKey]
	,[TCancelReasonKey]
	,[DateTimeScheduledFrom]
	,[DateTimeScheduledTo]
	,[IsStudentCheckedIn]
	,[IsCounselorVerified]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@SchoolKey
	,@JanetJonhesUserKey
	,@JohnLegendStudentKey
	,@TestUserKey
	,@PersonalProblemAppointmentTypeKey
	,NULL
	,NULL
	,DATEADD(MI, 45, DATEADD(HH, 15, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,DATEADD(MI, 45, DATEADD(HH, 16, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,1
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TAppointment]
	([TSchoolKey]
	,[CounselorTUserKey]
	,[TStudentKey]
	,[ScheduledByTUserKey]
	,[TAppointmentTypeKey]
	,[CanceledByTUserKey]
	,[TCancelReasonKey]
	,[DateTimeScheduledFrom]
	,[DateTimeScheduledTo]
	,[IsStudentCheckedIn]
	,[IsCounselorVerified]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@SchoolKey
	,@JamesFrancoUserKey
	,@JohnLegendStudentKey
	,@TestUserKey
	,@CollegeInformationAppointmentTypeKey
	,NULL
	,NULL
	,DATEADD(MI, 45, DATEADD(HH, 10, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,DATEADD(MI, 15, DATEADD(HH, 11, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,1
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')

INSERT INTO [dbo].[TAppointment]
	([TSchoolKey]
	,[CounselorTUserKey]
	,[TStudentKey]
	,[ScheduledByTUserKey]
	,[TAppointmentTypeKey]
	,[CanceledByTUserKey]
	,[TCancelReasonKey]
	,[DateTimeScheduledFrom]
	,[DateTimeScheduledTo]
	,[IsStudentCheckedIn]
	,[IsCounselorVerified]
	,[TStamp]
	,[DateCreated]
	,[CreatedBy]
	,[Source])
VALUES
	(@SchoolKey
	,@DeanSmithUserKey
	,@JohnLegendStudentKey
	,@TestUserKey
	,@GradesAppointmentTypeKey
	,NULL
	,NULL
	,DATEADD(MI, 20, DATEADD(HH, 12, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,DATEADD(MI, 45, DATEADD(HH, 12, DATEADD(DD, 0, DATEDIFF(DD, 0, GETDATE()))))
	,1
	,1
	,GETDATE()
	,GETDATE()
	,'Test'
	,'Test')
GO
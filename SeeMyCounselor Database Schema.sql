USE [master]
GO
/****** Object:  Database [SeeMyCounselorV4]    Script Date: 9/28/2015 9:34:09 PM ******/
CREATE DATABASE [SeeMyCounselorV4]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SeeMyCounselor', FILENAME = N'C:\Projects\Ignyte\SeeMyCounselor\Database\V4\SeeMyCounselor.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SeeMyCounselor_log', FILENAME = N'C:\Projects\Ignyte\SeeMyCounselor\Database\V4\SeeMyCounselor_log.ldf' , SIZE = 8384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SeeMyCounselorV4] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SeeMyCounselorV4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SeeMyCounselorV4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET ARITHABORT OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [SeeMyCounselorV4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SeeMyCounselorV4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SeeMyCounselorV4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SeeMyCounselorV4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SeeMyCounselorV4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET RECOVERY FULL 
GO
ALTER DATABASE [SeeMyCounselorV4] SET  MULTI_USER 
GO
ALTER DATABASE [SeeMyCounselorV4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SeeMyCounselorV4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SeeMyCounselorV4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SeeMyCounselorV4] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SeeMyCounselorV4', N'ON'
GO
USE [SeeMyCounselorV4]
GO
/****** Object:  Table [dbo].[TAppointment]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TAppointment](
	[Key] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[CounselorTUserKey] [uniqueidentifier] NOT NULL,
	[TStudentKey] [uniqueidentifier] NOT NULL,
	[ScheduledByTUserKey] [uniqueidentifier] NOT NULL,
	[TAppointmentTypeKey] [uniqueidentifier] NOT NULL,
	[CanceledByTUserKey] [uniqueidentifier] NULL,
	[TCancelReasonKey] [uniqueidentifier] NULL,
	[DateTimeScheduledFrom] [datetime] NOT NULL,
	[DateTimeScheduledTo] [datetime] NOT NULL,
	[IsStudentCheckedIn] [bit] NOT NULL,
	[IsCounselorVerified] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TAppointment] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TAppointmentType]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TAppointmentType](
	[Key] [uniqueidentifier] NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[TSupportedColorKey] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[LengthOfTimeInMinutes] [int] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TAppointmentType] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TCancelReason]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TCancelReason](
	[Key] [uniqueidentifier] NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](300) NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TCancelReason] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TCounselorBusyTime]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TCounselorBusyTime](
	[Key] [uniqueidentifier] NOT NULL,
	[TSchoolToTUserKey] [uniqueidentifier] NOT NULL,
	[BusyDateTimeFrom] [datetime] NOT NULL,
	[BusyDatTimeeTo] [datetime] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TCounselorBusyTime] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TRequest]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRequest](
	[Key] [uniqueidentifier] NOT NULL,
	[TStudentKey] [uniqueidentifier] NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[DateTimeRequestedFrom] [datetime] NOT NULL,
	[DateTimeRequestedTo] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TRequest] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TRole]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRole](
	[Key] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TRole] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSchool]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSchool](
	[Key] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[AddressLine1] [nvarchar](100) NOT NULL,
	[AddressLine2] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[StateOrProvince] [nvarchar](50) NOT NULL,
	[ZipOrPostalCode] [nvarchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TSchool] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSchoolToTUser]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSchoolToTUser](
	[Key] [uniqueidentifier] NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[TUserKey] [uniqueidentifier] NOT NULL,
	[TRoleKey] [uniqueidentifier] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TSchoolToTUser] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSetting]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSetting](
	[Key] [uniqueidentifier] NOT NULL,
	[TSchoolKey] [uniqueidentifier] NOT NULL,
	[ShouldVerifyCheckins] [bit] NOT NULL,
	[CounselorStartTime] [datetime] NOT NULL,
	[CounselorEndTime] [datetime] NOT NULL,
	[CanCounselorAcceptRequest] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TSettings] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TStudent]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TStudent](
	[Key] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[StudentIdentifier] [nvarchar](50) NOT NULL,
	[IsMale] [bit] NOT NULL,
	[CellPhone] [nchar](10) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[CanUseEmail] [bit] NOT NULL,
	[CanUseCellPhone] [bit] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TStudent] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TStudentToCounselor]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TStudentToCounselor](
	[Key] [uniqueidentifier] NOT NULL,
	[TStudentKey] [uniqueidentifier] NOT NULL,
	[CounselorTUserKey] [uniqueidentifier] NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TStudentToCounselor] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TSupportedColor]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TSupportedColor](
	[Key] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TAppointmentTypeColor] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TUser]    Script Date: 9/28/2015 9:34:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TUser](
	[Key] [uniqueidentifier] NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[TStamp] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[LastUpdatedBy] [nvarchar](50) NULL,
	[Source] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TUser] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_StudentCheckIn]  DEFAULT ((0)) FOR [IsStudentCheckedIn]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_CounselorCheckIn]  DEFAULT ((0)) FOR [IsCounselorVerified]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TAppointment] ADD  CONSTRAINT [DF_TAppointment_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TAppointmentType] ADD  CONSTRAINT [DF_TAppointmentType_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TAppointmentType] ADD  CONSTRAINT [DF_TAppointmentType_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TAppointmentType] ADD  CONSTRAINT [DF_TAppointmentType_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TAppointmentType] ADD  CONSTRAINT [DF_TAppointmentType_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TAppointmentType] ADD  CONSTRAINT [DF_TAppointmentType_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TCancelReason] ADD  CONSTRAINT [DF_TCancelReason_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TCancelReason] ADD  CONSTRAINT [DF_TCancelReason_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TCancelReason] ADD  CONSTRAINT [DF_TCancelReason_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TCancelReason] ADD  CONSTRAINT [DF_TCancelReason_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TCancelReason] ADD  CONSTRAINT [DF_TCancelReason_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TCounselorBusyTime] ADD  CONSTRAINT [DF_TCounselorBusyTime_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TCounselorBusyTime] ADD  CONSTRAINT [DF_TCounselorBusyTime_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TCounselorBusyTime] ADD  CONSTRAINT [DF_TCounselorBusyTime_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TCounselorBusyTime] ADD  CONSTRAINT [DF_TCounselorBusyTime_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TCounselorBusyTime] ADD  CONSTRAINT [DF_TCounselorBusyTime_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TRequest] ADD  CONSTRAINT [DF_TRequest_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TRequest] ADD  CONSTRAINT [DF_TRequest_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TRequest] ADD  CONSTRAINT [DF_TRequest_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TRequest] ADD  CONSTRAINT [DF_TRequest_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TRequest] ADD  CONSTRAINT [DF_TRequest_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TRole] ADD  CONSTRAINT [DF_TRole_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TRole] ADD  CONSTRAINT [DF_TRole_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TRole] ADD  CONSTRAINT [DF_TRole_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TRole] ADD  CONSTRAINT [DF_TRole_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TRole] ADD  CONSTRAINT [DF_TRole_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TSchool] ADD  CONSTRAINT [DF_TSchool_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TSchool] ADD  CONSTRAINT [DF_TSchool_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TSchool] ADD  CONSTRAINT [DF_TSchool_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TSchool] ADD  CONSTRAINT [DF_TSchool_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TSchool] ADD  CONSTRAINT [DF_TSchool_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TSchoolToTUser] ADD  CONSTRAINT [DF_TSchoolToTUser_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TSchoolToTUser] ADD  CONSTRAINT [DF_TSchoolToTUser_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TSchoolToTUser] ADD  CONSTRAINT [DF_TSchoolToTUser_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TSchoolToTUser] ADD  CONSTRAINT [DF_TSchoolToTUser_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TSchoolToTUser] ADD  CONSTRAINT [DF_TSchoolToTUser_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_TSettings_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_Settings_MeetingReminder]  DEFAULT ((1)) FOR [ShouldVerifyCheckins]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_TSettings_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_TSettings_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_TSettings_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TSetting] ADD  CONSTRAINT [DF_TSettings_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TStudent] ADD  CONSTRAINT [DF_TStudent_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TStudent] ADD  CONSTRAINT [DF_TStudent_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TStudent] ADD  CONSTRAINT [DF_TStudent_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TStudent] ADD  CONSTRAINT [DF_TStudent_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TStudent] ADD  CONSTRAINT [DF_TStudent_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TStudentToCounselor] ADD  CONSTRAINT [DF_TStudentToCounselor_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TStudentToCounselor] ADD  CONSTRAINT [DF_TStudentToCounselor_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TStudentToCounselor] ADD  CONSTRAINT [DF_TStudentToCounselor_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TStudentToCounselor] ADD  CONSTRAINT [DF_TStudentToCounselor_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TStudentToCounselor] ADD  CONSTRAINT [DF_TStudentToCounselor_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TSupportedColor] ADD  CONSTRAINT [DF_TAppointmentTypeColor_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TSupportedColor] ADD  CONSTRAINT [DF_TAppointmentTypeColor_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TSupportedColor] ADD  CONSTRAINT [DF_TAppointmentTypeColor_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TSupportedColor] ADD  CONSTRAINT [DF_TAppointmentTypeColor_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TSupportedColor] ADD  CONSTRAINT [DF_TAppointmentTypeColor_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_Key]  DEFAULT (newsequentialid()) FOR [Key]
GO
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_TStamp]  DEFAULT (getdate()) FOR [TStamp]
GO
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_DateCreated]  DEFAULT (getdate()) FOR [DateCreated]
GO
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TUser] ADD  CONSTRAINT [DF_TUser_Source]  DEFAULT (app_name()) FOR [Source]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TAppointmentType] FOREIGN KEY([TAppointmentTypeKey])
REFERENCES [dbo].[TAppointmentType] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TAppointmentType]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TCancelReason] FOREIGN KEY([TCancelReasonKey])
REFERENCES [dbo].[TCancelReason] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TCancelReason]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TSchool]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TStudent] FOREIGN KEY([TStudentKey])
REFERENCES [dbo].[TStudent] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TStudent]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TUser] FOREIGN KEY([CounselorTUserKey])
REFERENCES [dbo].[TUser] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TUser]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TUser_CancelledBy] FOREIGN KEY([CanceledByTUserKey])
REFERENCES [dbo].[TUser] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TUser_CancelledBy]
GO
ALTER TABLE [dbo].[TAppointment]  WITH CHECK ADD  CONSTRAINT [FK_TAppointment_TUser_ScheduledBy] FOREIGN KEY([ScheduledByTUserKey])
REFERENCES [dbo].[TUser] ([Key])
GO
ALTER TABLE [dbo].[TAppointment] CHECK CONSTRAINT [FK_TAppointment_TUser_ScheduledBy]
GO
ALTER TABLE [dbo].[TAppointmentType]  WITH CHECK ADD  CONSTRAINT [FK_TAppointmentType_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TAppointmentType] CHECK CONSTRAINT [FK_TAppointmentType_TSchool]
GO
ALTER TABLE [dbo].[TAppointmentType]  WITH CHECK ADD  CONSTRAINT [FK_TAppointmentType_TSupportedColor] FOREIGN KEY([TSupportedColorKey])
REFERENCES [dbo].[TSupportedColor] ([Key])
GO
ALTER TABLE [dbo].[TAppointmentType] CHECK CONSTRAINT [FK_TAppointmentType_TSupportedColor]
GO
ALTER TABLE [dbo].[TCancelReason]  WITH CHECK ADD  CONSTRAINT [FK_TCancelReason_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TCancelReason] CHECK CONSTRAINT [FK_TCancelReason_TSchool]
GO
ALTER TABLE [dbo].[TCounselorBusyTime]  WITH CHECK ADD  CONSTRAINT [FK_TCounselorBusyTime_TSchoolToTUser] FOREIGN KEY([TSchoolToTUserKey])
REFERENCES [dbo].[TSchoolToTUser] ([Key])
GO
ALTER TABLE [dbo].[TCounselorBusyTime] CHECK CONSTRAINT [FK_TCounselorBusyTime_TSchoolToTUser]
GO
ALTER TABLE [dbo].[TRequest]  WITH CHECK ADD  CONSTRAINT [FK_TRequest_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TRequest] CHECK CONSTRAINT [FK_TRequest_TSchool]
GO
ALTER TABLE [dbo].[TRequest]  WITH CHECK ADD  CONSTRAINT [FK_TRequest_TStudent] FOREIGN KEY([TStudentKey])
REFERENCES [dbo].[TStudent] ([Key])
GO
ALTER TABLE [dbo].[TRequest] CHECK CONSTRAINT [FK_TRequest_TStudent]
GO
ALTER TABLE [dbo].[TSchoolToTUser]  WITH CHECK ADD  CONSTRAINT [FK_TSchoolToTUser_TRole] FOREIGN KEY([TRoleKey])
REFERENCES [dbo].[TRole] ([Key])
GO
ALTER TABLE [dbo].[TSchoolToTUser] CHECK CONSTRAINT [FK_TSchoolToTUser_TRole]
GO
ALTER TABLE [dbo].[TSchoolToTUser]  WITH CHECK ADD  CONSTRAINT [FK_TSchoolToTUser_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TSchoolToTUser] CHECK CONSTRAINT [FK_TSchoolToTUser_TSchool]
GO
ALTER TABLE [dbo].[TSchoolToTUser]  WITH CHECK ADD  CONSTRAINT [FK_TSchoolToTUser_TUser] FOREIGN KEY([TUserKey])
REFERENCES [dbo].[TUser] ([Key])
GO
ALTER TABLE [dbo].[TSchoolToTUser] CHECK CONSTRAINT [FK_TSchoolToTUser_TUser]
GO
ALTER TABLE [dbo].[TSetting]  WITH CHECK ADD  CONSTRAINT [FK_TSetting_TSchool] FOREIGN KEY([TSchoolKey])
REFERENCES [dbo].[TSchool] ([Key])
GO
ALTER TABLE [dbo].[TSetting] CHECK CONSTRAINT [FK_TSetting_TSchool]
GO
ALTER TABLE [dbo].[TStudentToCounselor]  WITH CHECK ADD  CONSTRAINT [FK_TStudentToCounselor_TStudent] FOREIGN KEY([TStudentKey])
REFERENCES [dbo].[TStudent] ([Key])
GO
ALTER TABLE [dbo].[TStudentToCounselor] CHECK CONSTRAINT [FK_TStudentToCounselor_TStudent]
GO
ALTER TABLE [dbo].[TStudentToCounselor]  WITH CHECK ADD  CONSTRAINT [FK_TStudentToCounselor_TUser] FOREIGN KEY([CounselorTUserKey])
REFERENCES [dbo].[TUser] ([Key])
GO
ALTER TABLE [dbo].[TStudentToCounselor] CHECK CONSTRAINT [FK_TStudentToCounselor_TUser]
GO
USE [master]
GO
ALTER DATABASE [SeeMyCounselorV4] SET  READ_WRITE 
GO
